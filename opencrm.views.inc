<?php
/**
 * @file
 * Views integration

/**
 * Implements hook_views_data().
 */
function opencrm_views_data() {
  $data = array();

  $data['party']['opencrm_edit_link'] = array(
    'title' => t('OpenCRM Edit Link'),
    'help' => t('Provide a link to the party edit form.'),
    'field' => array(
      'real field' => 'pid',
      'handler' => 'opencrm_handler_field_edit_link',
    ),
  );

  $data['party']['opencrm_join_link'] = array(
    'title' => t('OpenCRM Join Link'),
    'help' => t('Provide a link to the party join form.'),
    'field' => array(
      'real field' => 'pid',
      'handler' => 'opencrm_handler_field_join_link',
    ),
  );

  $data['party']['opencrm_leave_link'] = array(
    'title' => t('OpenCRM Leave Link'),
    'help' => t('Provide a link to the party leave form.'),
    'field' => array(
      'real field' => 'pid',
      'handler' => 'opencrm_handler_field_leave_link',
    ),
  );

  $data['party_attached_entity']['edit'] = array(
    'title' => t('Edit link'),
    'help' => t('Provide a link to edit the attached entity.'),
    'field' => array(
      'real field' => 'pid',
      'op' => 'edit',
      'handler' => 'opencrm_handler_field_attached_entity_link',
    ),
  );

  $data['party_attached_entity']['remove'] = array(
    'title' => t('Remove link'),
    'help' => t('Provide a link to remove the attached entity.'),
    'field' => array(
      'real field' => 'pid',
      'op' => 'detach',
      'handler' => 'opencrm_handler_field_attached_entity_link',
    ),
  );

  return $data;
}

/**
 * Implements hook_views_data_alter().
 */
function opencrm_views_data_alter(&$data) {
  // Adjust the pid field on the party.
  $data['party']['pid']['argument']['handler'] = 'opencrm_handler_argument_party';

  // Find all entity reference fields that target parties.
  foreach (field_info_fields() as $field_info) {
    // We're only interested in entityreferences.
    if ($field_info['type'] == 'entityreference') {
      // Only ones that reference parties are relevant.
      if ($field_info['settings']['target_type'] == 'party') {
        // Now we need to adjust the argument handler.
        $table_name = 'field_data_' . $field_info['field_name'];
        $field_name = $field_info['field_name'] . '_target_id';
        if (isset($data[$table_name][$field_name]['argument']['handler'])) {
          $data[$table_name][$field_name]['argument']['handler'] = 'opencrm_handler_argument_party';
        }
      }
    }
  }

  // Provide backwards compatibility for previous version of Search API VBO.
  foreach (search_api_index_load_multiple(FALSE) as $index) {
    if ($entity_type = $index->getEntityType()) {
      $data['search_api_index_' . $index->machine_name]['views_bulk_operations'] = array(
        'moved to' => array('views_entity_' . $entity_type, 'views_bulk_operations'),
      );
    }
  }
}

/**
 * Implements hook_views_query_alter().
 */
function opencrm_views_query_alter(&$view, &$query) {
  // On the duplicates view, make sure we do not show copies of the same party
  // in the listings.
  if ($view->name == 'party_view__duplicates') {
    $party_id = $view->argument['pid']->argument;
    $query->where[0]['conditions'][] = array(
      'field' => 'party.pid',
      'value' => $party_id,
      'operator' => '<>',
    );
  }
}

/**
 * Implements hook_views_pre_view().
 */
function opencrm_views_pre_view(&$view, &$display_id, &$args) {
  if ($view->name == 'opencrm_party_entity_reference_solr') {
    $options = $view->display_handler->get_option('entityreference_options');
    if (!empty($options['ids'])) {
      $options['solr_ids'] = $options['ids'];
      $options['ids'] = NULL;
    }
    $view->display_handler->set_option('entityreference_options', $options);
  }
}

/**
 * Implements hook_views_pre_render().
 */
function opencrm_views_pre_render(&$view) {
  if (substr($view->base_table, 0, 16) == 'search_api_index') {
    foreach ($view->result as &$result) {
      $result->search_api_id = $result->_entity_properties['search_api_id'];
    }
  }
}
