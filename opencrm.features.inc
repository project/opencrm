<?php
/**
 * @file
 */

/**
 * Implements hook_views_api().
 */
function opencrm_views_api() {
  return array("api" => "3.0");
}

/**
 * Implements hook_default_search_api_index().
 */
function opencrm_default_search_api_index() {
  $items = array();

  // Special Configurable Server
  $server = variable_get('opencrm_search_api_server', '');
  $items['party_index'] = entity_import('search_api_index', '{
    "name" : "Party Index",
    "machine_name" : "party_index",
    "description" : "SOLR Index of parties for the Party Dashboard.",
    "server" : ' . ($server ? '"' . $server . '"' : 'null') . ',
    "item_type" : "party",
    "options" : {
      "index_directly" : 1,
      "cron_limit" : "0",
      "fields" : {
        "pid" : { "type" : "integer" },
        "label" : { "type" : "string" },
        "email" : { "type" : "text", "boost" : "5.0", "real_type" : "emailurl_partial" },
        "archived" : { "type" : "boolean" },
        "user" : { "type" : "integer", "entity_type" : "user" },
        "party_hat" : { "type" : "list\\u003Cstring\\u003E", "entity_type" : "party_hat" },
        "party_needs_review" : { "type" : "boolean" },
        "search_api_language" : { "type" : "string" },
        "label_fulltext" : { "type" : "text", "boost" : "8.0", "real_type" : "text_partial" },
        "search_api_aggregation_1" : { "type" : "text", "boost" : "21.0" },
        "profile2_party_indiv:party_indiv_email" : { "type" : "text", "boost" : "2.0", "real_type" : "text_partial" },
        "profile2_party_org:party_org_email" : { "type" : "text", "boost" : "2.0", "real_type" : "text_partial" },
        "party_simplenews_subscriber:simplenews_subscriptions" : {
          "type" : "list\\u003Cinteger\\u003E",
          "entity_type" : "simplenews_newsletter"
        },
        "user:uid" : { "type" : "integer" },
        "user:name" : { "type" : "text", "boost" : "5.0", "real_type" : "text_partial" },
        "user:mail" : { "type" : "text", "boost" : "2.0", "real_type" : "email_partial" },
        "profile2_party_indiv:party_indiv_name:given" : { "type" : "text", "boost" : "13.0", "real_type" : "text_partial" },
        "profile2_party_indiv:party_indiv_name:family" : { "type" : "text", "boost" : "13.0", "real_type" : "text_partial" }
      },
      "data_alter_callbacks" : {
        "party_alter_status_filter" : { "status" : 1, "weight" : "-10", "settings" : [] },
        "search_api_alter_add_viewed_entity" : { "status" : 0, "weight" : "0", "settings" : { "mode" : "full" } },
        "search_api_alter_add_url" : { "status" : 0, "weight" : "0", "settings" : [] },
        "search_api_alter_add_aggregation" : {
          "status" : 1,
          "weight" : "0",
          "settings" : { "fields" : {
              "label_fulltext" : {
                "name" : "Label (Fulltext)",
                "type" : "fulltext",
                "fields" : [ "label" ],
                "description" : "A Fulltext aggregation of the following fields: Label."
              },
              "search_api_aggregation_1" : {
                "name" : "PID (Fulltext)",
                "type" : "fulltext",
                "fields" : [ "pid" ],
                "description" : "A Fulltext aggregation of the following fields: Party ID."
              }
            }
          }
        }
      },
      "processors" : {
        "search_api_case_ignore" : {
          "status" : 1,
          "weight" : "0",
          "settings" : { "fields" : { "label_fulltext" : true, "user:name" : true, "user:mail" : true } }
        },
        "search_api_html_filter" : {
          "status" : 0,
          "weight" : "10",
          "settings" : {
            "fields" : {
              "label_fulltext" : true,
              "profile2_party_indiv:party_indiv_email" : true,
              "user:name" : true,
              "user:mail" : true
            },
            "title" : 0,
            "alt" : 1,
            "tags" : "h1 = 5\\r\\nh2 = 3\\r\\nh3 = 2\\r\\nstrong = 2\\r\\nb = 2\\r\\nem = 1.5\\r\\nu = 1.5"
          }
        },
        "search_api_tokenizer" : {
          "status" : 0,
          "weight" : "20",
          "settings" : {
            "fields" : { "label_fulltext" : true, "user:name" : true, "user:mail" : true },
            "spaces" : "[^[:alnum:]]",
            "ignorable" : "[\\u0027]"
          }
        },
        "search_api_stopwords" : {
          "status" : 0,
          "weight" : "30",
          "settings" : {
            "fields" : {
              "label_fulltext" : true,
              "profile2_party_indiv:party_indiv_email" : true,
              "user:name" : true,
              "user:mail" : true
            },
            "file" : "",
            "stopwords" : "but\\r\\ndid\\r\\nthe this that those\\r\\netc"
          }
        }
      }
    },
    "enabled" : "' . ($server ? 1 : 0) . '",
    "read_only" : "0"
  }');

  return $items;
}
