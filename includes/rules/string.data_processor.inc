<?php

/**
 * String Manipulation Data Processor Class.
 */
class OpenCRMRulesDataProcessorString extends RulesDataProcessor {

  public function process($value, $info, RulesState $state, RulesPlugin $element) {
    $value = isset($this->processor) ? $this->processor->process($value, $info, $state, $element) : $value;

    $str_first = array('substr', 'ucwords');
    $str_last = array('str_replace', 'preg_replace');

    $function = $this->setting['function'];
    if (is_callable($function)) {
      $args = !empty($this->setting['arguments']) ? str_getcsv($this->setting['arguments']) : array();

      if (in_array($function, $str_first)) {
        array_unshift($args, $value);
      }
      else {
        array_push($args, $value);
      }

      $value = call_user_func_array($function, $args);
    }

    return $value;
  }

  protected static function form($settings, $var_info) {
    $settings += array('function' => '', 'arguments' => '');
    $form = array(
      '#type' => 'fieldset',
      '#title' => t('Manipulate String'),
      '#collapsible' => TRUE,
      '#collapsed' => empty($settings['function']),
      '#description' => t('Make alterations to a string of text.'),
    );
    $form['function'] = array(
      '#type' => 'select',
      '#title' => t('Function'),
      '#description' => t('The function to run on the string.'),
      '#options' => array(
        'preg_replace' => 'preg_replace - Replace all matches of a regular expression with some string.',
        'str_replace' => 'str_replace - Replace all occurences of some string with another string.',
        'substr' => 'substr - Return part of a string.',
        'ucwords' => 'ucwords - Capitalize first letter of each word.',
      ),
      '#empty_option' => t('- None -'),
      '#default_value' => $settings['function'],
    );
    $form['arguments'] = array(
      '#type' => 'textfield',
      '#title' => t('Additional Arguments'),
      '#description' => t('Add any additional arguments required for the function as a comma separated list.'),
      '#default_value' => $settings['arguments'],
    );
    return $form;
  }
}
