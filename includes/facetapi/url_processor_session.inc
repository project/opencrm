<?php

/**
 * @file
 * A facet handler that falls back onto session set variables.
 */

/**
 * Url processor plugin that retrieves facet data from the query string.
 *
 * This plugin retrieves facet data from $_SESSION. $_SESSION is populated
 * from $_GET, storing all information in the "f" query string variable by
 * default.
 */
class FacetapiUrlProcessorSession extends FacetapiUrlProcessorStandard {

  /**
   * Stores the calculated namespace for this request keyed by searcher.
   *
   * var array
   */
  static protected $namespaces;

  /**
   * Implements FacetapiUrlProcessor::fetchParams().
   *
   * Use $_GET as the source for facet data when available and fall back to
   * $_SESSION, also storing the $_GET bits into the session.
   */
  public function fetchParams() {
    // Get hold of our namespaced session variable.
    $session =& $this->getSession();

    // Check if we need to update the session with a new request.
    if (isset($_GET[$this->filterKey])) {
      $session[$this->filterKey] = $_GET[$this->filterKey] == 'clear' ? array(): $_GET[$this->filterKey];

      // Strip out any empty filters
      foreach ($session[$this->filterKey] as $i => $facet) {
        $parts = explode(':', $facet);
        if (!isset($parts[1])) {
          unset($session[$this->filterKey][$i]);
        }
      }
    }

    return $session;
  }

  /**
   * Implements FacetapiUrlProcessor::setBreadcrumb().
   */
  public function setBreadcrumb() {
    // Do nothing.
  }

  /**
   * Implements FacetapiUrlProcessor::getQueryString().
   */
  public function getQueryString(array $facet, array $values, $active) {
    // Let our parent do the processing.
    $query = parent::getQueryString($facet, $values, $active);

    // We need a special way to clear facets stored in session.
    if (empty($query[$this->filterKey])) {
      $query[$this->filterKey][0] = 'clear';
    }

    return $query;
  }

  /**
   * Get the namespaced session variable.
   */
  protected function &getSession() {
    $namespace = self::getNamespace($this->adapter->getSearcher());

    // If we have a namespace, return the session variable by reference.
    if ($namespace) {
      // We also namespace by adapter to prevent un-wanted crossover.
      if (!isset($_SESSION['facetapi']['url_processor_session'][$this->adapter->getSearcher()][$namespace])) {
        $_SESSION['facetapi']['url_processor_session'][$this->adapter->getSearcher()][$namespace] = array();
      }
      return $_SESSION['facetapi']['url_processor_session'][$this->adapter->getSearcher()][$namespace];
    }

    // Otherwise create a variable we can return by reference.
    $session = array();
    return $session;
  }

  /**
   * Sniff out a namespace.
   *
   * @param string $searcher
   *   The name of the searcher we want the namespace for.
   */
  static public function getNamespace($searcher) {
    if (!isset(self::$namespaces[$searcher])) {
      // Set it to false in case we don't find one.
      self::$namespaces[$searcher] = FALSE;
      $namespace = &self::$namespaces[$searcher];

      // Get hold of our patterns.
      $patterns = opencrm_get_facet_namespace_patterns($searcher);

      // Iterate over our patterns in order and take the first match.
      foreach ($patterns as $name => $pattern) {
        if (drupal_match_path(current_path(), $pattern)) {
          $namespace = $name;
          break;
        }
      }

      if ($pos = strpos($namespace, '|') !== FALSE) {
        if ($parts = intval(substr($namespace, $pos + 1))) {
          $namespace = $namespace . implode('/', array_slice(arg(), 0, $parts));
        }
      }
    }

    return self::$namespaces[$searcher];
  }

  /**
   * Set the namespace.
   *
   * @param string $searcher
   *   The searcher we're setting the namespace for.
   * @param string $namespace
   *   The namespace to set.
   */
  static public function setNamespace($searcher, $namespace) {
    self::$namespaces[$searcher] = $namespace;
  }

  /**
   * Clear the session.
   *
   * @param array|NULL $searchers
   *   An array of searcher names that we want to clear. If NULL we'll clear
   *   them all.
   * @param array|TRUE|NULL $namespaces
   *   Leave NULL to get the current namespace, TRUE to clear all or specify
   *   a namespace as an array of strings to target a specific ones.
   */
  static public function clearSession($searchers = NULL, $namespaces = NULL) {
    // Shortcut it all if we want to clear absolutely everything.
    if (!$searchers && $namespaces === TRUE) {
      unset($_SESSION['facetapi']['url_processor_session']);
      return;
    }

    // If clearing all searchers, get hold of a list.
    if (!$searchers) {
      $searchers = array_keys(facetapi_get_searcher_info());
    }

    // Iterate over the searchers clearing out as specified.
    foreach ($searchers as $searcher) {
      // If we're clearing everything, let's go.
      if ($namespaces === TRUE) {
        // Entirely clear this searcher.
        unset($_SESSION['facetapi']['url_processor_session'][$searcher]);
      }
      // If we have an array, let's work through them.
      elseif (is_array($namespaces)) {
        foreach ($namespaces as $namespace) {
          unset($_SESSION['facetapi']['url_processor_session'][$searcher][$namespace]);
        }
      }
      // Otherwise we want to find the current namespace.
      else {
        $namespace = self::getNamespace($searcher);
        unset($_SESSION['facetapi']['url_processor_session'][$searcher][$namespace]);
      }
    }
  }

}
