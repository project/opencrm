<?php
/**
 * @file
 * Contains a Views field handler to take care of displaying deletes links
 * as fields
 */

class opencrm_handler_field_edit_link extends opencrm_handler_field_link {
  function render($values) {
    $party_id = $values->{$this->aliases['pid']};
    $party = party_load($party_id);

    return opencrm_get_edit_link($party, array('fallback' => TRUE));
  }
}
