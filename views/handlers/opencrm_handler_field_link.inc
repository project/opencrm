<?php
/**
 * @file
 * Contains a Views field handler to take care of displaying links
 * as fields
 */

class opencrm_handler_field_link extends views_handler_field {
  function construct() {
    parent::construct();

    $this->additional_fields['pid'] = 'pid';
  }

  function option_definition() {
    $options = parent::option_definition();

    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
  }

  function query() {
    $this->ensure_my_table();
    $this->add_additional_fields();
  }

  function render($values) {
    return FALSE;
  }
}
