<?php
/**
 * Views handler for parties.
 */

class opencrm_handler_argument_party extends views_handler_argument_numeric {

  function option_definition() {
    $options = parent::option_definition();

    $options['convert_from_user'] = array(
      'default' => FALSE,
      'bool' => TRUE,
    );

    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['convert_from_user'] = array(
      '#type' => 'checkbox',
      '#title' => t('Convert to a Party from a User'),
      '#description' => t('If selected, IDs received will be converted from a user id to a party id based off of the first party found for the user.'),
      '#default_value' => !empty($this->options['convert_from_user']),
      '#fieldset' => 'more',
    );
  }

  function set_argument($arg) {
    // If requested, convert this into a party id.
    if (!empty($this->options['convert_from_user'])) {
      if ($user = user_load($arg)) {
        $party = party_user_get_party($user);
      }

      $arg = !empty($party) ? $party->pid: NULL;
    }

    return parent::set_argument($arg);
  }

}
