<?php
/**
 * @file
 * Contains a Views field handler to take care of displaying edit links
 * as fields
 */

class opencrm_handler_field_attached_entity_link extends views_handler_field {
  function construct() {
    parent::construct();

    $this->additional_fields['pid'] = 'pid';
    $this->additional_fields['delta'] = 'delta';
    $this->additional_fields['data_set'] = 'data_set';
  }

  function option_definition() {
    $options = parent::option_definition();

    $options['text'] = array('default' => '', 'translatable' => TRUE);

    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['text'] = array(
      '#type' => 'textfield',
      '#title' => t('Text to display'),
      '#default_value' => $this->options['text'],
    );
  }

  function query() {
    $this->ensure_my_table();
    $this->add_additional_fields();
  }

  function render($values) {
    // Add ctools js.
    ctools_include('ajax');
    ctools_include('modal');
    ctools_modal_add_js();

    $pid = $this->get_value($values, 'pid');
    $data_set = $this->get_value($values, 'data_set');
    $data_set_info = party_get_data_set_info($data_set);
    $delta = $this->get_value($values, 'delta');
    $op = $this->definition['op'];

    // Loading party to check access against.
    $party = party_load($pid);
    if (!party_access($op, $party, $data_set)) {
      return;
    }

    switch ($op) {
      default:
        return;
      case 'edit':
        $text = t('edit');
        $path = 'admin/party/nojs/attached-entity/' . $data_set_info['path element'] . '/' . $pid . '/' . $delta;
        break;
      case 'detach':
        $text = t('remove');
        $path = 'admin/party/nojs/attached-entity/' . $data_set_info['path element'] . '/' . $pid . '/' . $delta . '/remove';
        break;
    }

    $text = !empty($this->options['text']) ? $this->options['text'] : $text;

    return l($text, $path, array(
      'query' => drupal_get_destination(),
      'attributes' => array('class' => 'ctools-use-modal')
    ));
  }
}
