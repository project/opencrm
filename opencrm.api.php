<?php
/**
 * @file
 * API documenations for Party Open CRM.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Define namespaces for the Facet API Session URL processor.
 *
 * @return array
 *   An array of paths suitable for drupal_match_path() keyed by the namespace
 *   identifier, outer keyed by the searcher they apply to. Global namespaces
 *   can be declared using an outer key of NULL. The namespace identified is
 *   comprised of alphanumeric characters and underscores. It can end with |INT
 *   where INT is an integer value indicating the namespace is sensitive the
 *   first INT parts of the path.
 *
 * @see hook_opencrm_facet_namespace_patterns_alter()
 */
function hook_opencrm_facet_namespace_patterns() {
  return array(
    'search_api@default_node_index' => array(
      'content_browser' => "node",
      'content_admin' => "admin/content\nadmin/content/*",
    ),
  );
}

/**
 * Alter namespaces for the Facet API Session URL processor.
 *
 * @param array $namespaces
 *   An array of paths suitable for drupal_match_path() keyed by the namespace
 *   identifier, outer keyed by the searcher they apply to. Global namespaces
 *   use an outer key of NULL.
 *
 * @see hook_opencrm_facet_namespace_patterns()
 */
function hook_opencrm_facet_namespace_patterns_alter(&$namespaces) {
  // Make all admin facets remembered in the session.
  $namespaces[NULL]['admin'] = "admin\nadmin/*";
}

/**
 * Provide information for the party archive form.
 *
 * @param Party $party
 *   The party the archive form is for.
 * @param boolean $archiving
 *   TRUE if the party is being archived, FALSE if it's being unarchived.
 *
 * @return array
 *   An array of information items suitable for theme_item_list(). Keys are
 *   recommended to ease altering. Outer keyed by one of the following
 *   categories (in order of display):
 *     - info: Information that is helpful to know about archiving.
 *     - warnings: Warning that will be displayed in red.
 *     - actions: Actions that the user should take before archiving a party.
 *       If an item is passed as an array, the following keys can be set:
 *         - #required: Indicates the action is required.
 *         - #completed: Indicates the action has been completed.
 *       If an action is required but not completed the submit button will be
 *       disabled.
 *     - auto: Automated processes that will happen when a party is archived.
 *
 * @see hook_opencrm_archive_info_alter()
 */
function hook_opencrm_archive_info($party, $archiving) {
  if ($archiving) {
    $has_web_account = count($party->getDataSetController('user')->getEntityIds()) == 0;
    return array(
      'info' => array(
        'hide' => t('Archiving this party will hide it for users without the view archived parties permission.'),
      ),
      'warning' => array(
        'comms' => t('Communications with this party will be prevented.'),
      ),
      'actions' => array(
        'check' => t('Make sure the party should be archived.'),
        'account' => array(
          'data' => t('Block, delete or detach any connected web accounts.'),
          '#required' => TRUE,
          '#completed' => $has_web_account,
        ),
      ),
    );
  }
}

/**
 * Alter information for the party archive form.
 *
 * @param array $info
 *   An array of item list arrays to be outputted.
 * @param Party $party
 *   The party the archive form is for.
 * @param boolean $archiving
 *   TRUE if the party is being archived, FALSE if it's being unarchived.
 *
 * @see hook_opencrm_archive_info()
 */
function hook_opencrm_archive_info_alter(&$info, $party, $archiving) {
  if ($archiving) {
    $info['actions']['opencrm_account']['#required'] = FALSE;
  }
}

/**
 * Provide suggested text formats.
 *
 * @return array
 *   An array of text format suggestions outer keyed by the 'type' the
 *   suggestions relate to.
 *
 * @see opencrm_text_format()
 * @see hook_opencrm_text_format_alter()
 */
function hook_opencrm_text_formats() {
  return array(
    'html' => array(
      'full_html', // Provided by the core standard install profile.
      'panopoly_html_text', // Provided by panopoly_wysiwyg.
    ),
    'filtered' => array(
      'filtered_html', // Provided by the core standard install profile.
      'panopoly_wysiwyg_text', // Provided by panopoly_wysiwyg.
    ),
    'wysiwyg' => array(
      'panopoly_wysiwyg_text', // Provided by panopoly_wysiwyg.
    ),
  );
}

/**
 * Alter suggested text formats.
 *
 * @params array $suggestions
 *   An array of text format suggestions outer keyed by the 'type' the
 *   suggestions relate to.
 *
 * @see opencrm_text_format()
 * @see hook_opencrm_text_format()
 */
function hook_opencrm_text_formats_alter(&$suggestions) {
  // Override the wysiwyg to always use the one we provide.
  $suggestions['wysiwyg'] = array('my_module_wysiwyg');
}

/**
 * Inform OpenCRM of fields that should be synchronised.
 *
 * @return array
 *   An array outer keyed by target field name. Inner arrays are:
 *   - source: The source field name.
 *   - source entity_type: The entity type of the source instance. Not requrred
 *     if only synchronising field settings.
 *   - source bundle: The bundle of the source instance. Not requrred if only
 *     synchronising field settings.
 *   - field: Boolean indicating whether to synchronise field settings.
 *     Defaults to TRUE unless the source and target are the same.
 *   - bundles: An array of bundles outer keyed by entity type that should be
 *     synchronised. Defaults to all bundles on all entities.
 *
 * @see opencrm_field_sync_fields()
 * @see hook_opencrm_field_sync_fields_alter()
 */
function hook_opencrm_field_sync_fields() {
  return array(
    // Synchronise all instances of party_indiv_name.
    'party_indiv_name' => array(
      'source' => 'party_indiv_name',
    ),

    // Keep ticket_holder_name and the instance on ticket:ticket synchronised
    // with party_indiv_name.
    'ticket_holder_name' => array(
      'source' => 'party_indiv_name',
      'bundles' => array(
        'ticket' => array('ticket'),
      ),
    ),

    // Keep order_name instance on commerce_order:commerce_order synchronised
    // with party_indiv_name.
    'order_name' => array(
      'source' => 'party_indiv_name',
      'field' => FALSE,
      'bundles' => array(
        'commerce_order' => array('commerce_order'),
      ),
    ),
  );
}

/**
 * Alter the list of fields that OpenCRM should keep synchronised.
 *
 * @params array $fields
 *   See hook_opencrm_field_sync_fields()'s return for the array structure.
 *
 * @see opencrm_field_sync_fields()
 * @see hook_opencrm_field_sync_fields()
 */
function hook_opencrm_field_sync_fields_alter(&$fields) {
  // No example.
}

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Add modal links to pane titles.
 *
 * @param array $context
 *   An array of context containing:
 *   - pane: The pane we are adding links to.
 *   - args: The raw arguments for the panes.
 *   - context: The CTools context for the pane.
 *
 * @return array
 *   An array of links. Each link is an array containing:
 *   - url: The url for the link.
 *   - text: The text for the link.
 *   - alt: The alt text for the link.
 *
 * @see hook_party_dashboard_tools_modal_pane_alter()
 * @see party_dashboard_tools_panels_pane_content_alter()
 */
function hook_opencrm_modal_pane_links($context) {
  $links = array();

  if (isset($context['pane']->type) && $context['pane']->type == 'attached_entity') {
    $data_sets = party_get_data_set_info();
    $data_set = $data_sets[$context['pane']->subtype]['path element'];
    $pid = $context['args'][0];
    $delta = 0;

    // Append modal edit link to title.
    $links['edit'] = array(
      'url' => 'party/modal/' . $pid . '/' . $data_set . '/' . $delta . '/nojs',
      'text' => t('Edit'),
      'alt' => t('Edit attached entity'),
    );
  }

  return $links;
}

/**
 * Alter modal links for pane titles.
 *
 * @param array $links
 *   The links to be altered. See hook_party_dashboard_tools_modal_pane() for
 *   details.
 * @param array $context
 *   An array of context containing:
 *   - pane: The pane we are adding links to.
 *   - args: The raw arguments for the panes.
 *   - context: The CTools context for the pane.
 *
 * @see hook_party_dashboard_tools_modal_pane_alter()
 * @see party_dashboard_tools_panels_pane_content_alter()
 */
function hook_opencrm_modal_pane_links_alter(&$links, $context) {
  if (isset($links['edit'])) {
    $links['edit']['text'] = t('Adjust');
  }
}

/**
 * Define the available search portals.
 *
 * @return array
 *   An array of search portals, keyed by machine name with the following keys:
 *   - title: The translated title of the search portal.
 *   - category: (Optional) The translated category for the content type.
 *   - path: The path for the search page.
 *   - index: The Search API Index this portal uses.
 *   - search_key: The key for the full-text search.
 *
 * @see hook_opencrm_search_portal_types_alter()
 */
function hook_opencrm_search_portal_types() {
  $types = array();

  $types['party_dashboard'] = array(
    'title' => t('Party dashboard'),
    'category' => t('OpenCRM'),
    'path' => 'admin/party',
    'index' => 'party_index',
    'search_key' => 'search'
  );

  return $types;
}

/**
 * Alter the available search portals.
 *
 * @param $types
 *   The array of portal.
 *
 * @see hook_opencrm_search_portal_types()
 */
function hook_opencrm_search_portal_types_alter(&$types) {
  $types['party_dashboard']['title'] = t('Parties');
}

/**
 * Declare organisation reference fields that should be simplified.
 *
 * @return array
 *   An array of fields, each an array containing:
 *   - field: The machine name of the field.
 *   - entity types: An array of entity types to apply to. If NULL, it will
 *     apply to all bundles of all entity types. Entity types form keys and
 *     values are either NULL for all bundles or an array of bundles.
 *   - bypass permission: An array of permissions that will bypass the
 *     simplification. If NULL, it will always apply. Defaults to NULL.
 *   - allow change: Boolean indicating wither changes should be allowed on
 *     already saved entities. Defaults to TRUE.
 *   - allow change description: A untranslated message to append to the
 *     description if not allowed to change.
 *   - description: An untranslated message to append to the description if
 *     allowed to change.
 *   - permissions: An array of group permissions to check against. Required if
 *     roles not given.
 *   - roles: An array of group roles to check against. Required if permissions
 *     not given.
 */
function hook_opencrm_org_reference_simplify() {
  return array(
    array(
      'field' => 'field_org_ref',
      'entity types' => array(
        'node' => NULL, // All bundles of this entity type.
      ),
      'bypass permission' => array('bypass node access'),
      'allow change' => FALSE,
      'allow change description' => 'You can only set the organisation on content creation.',
      'description' => 'Please join an organisation if you need to add content for it.',
      'roles' => array('member'),
    ),
  );
}

/**
 * Alter organisation reference field simplification.
 *
 * @param array $fields
 *   The fields for simplification. See the return of
 *   hook_opencrm_org_reference_simplify() for the structure.
 */
function hook_opencrm_org_reference_simplify_alter(&$fields) {
  // Don't allow the organisation to be changed for on the basic node type.
  $fields['node:basic:field_org_ref']['allow change'] = TRUE;

  // Switch to checking the update group permission on all bundles.
  foreach ($fields as &$field) {
    if ($field['field'] == 'field_org_ref' && $field['entity type'] == 'node') {
      $fields['permissions'] = array('update group');
    }
  }
}

/**
 * @} End of "addtogroup hooks"
 */
