<?php
/**
 * @file
 * opencrm.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_pages().
 */
function opencrm_default_page_manager_pages() {
  $export = array();

  // Scan directory for any .panel files
  $files = file_scan_directory(dirname(__FILE__) . '/page_manager_pages', '/\.page$/', array('key' => 'name'));
  foreach ($files as $file) {
    if ((include $file->uri) == 1) {
      $export[$page->name] = $page;
    }
  }

  return $export;
}

/**
 * Implementation of hook_default_page_manager_handlers().
 */
function opencrm_default_page_manager_handlers() {
  $export = array();

  // Scan directory for any .panel files
  $files = file_scan_directory(dirname(__FILE__) . '/page_manager_handlers', '/\.panel$/', array('key' => 'name'));
  foreach ($files as $file) {
    if ((include $file->uri) == 1) {
      $export[$handler->name] = $handler;
    }
  }

  return $export;
}
