<?php
/**
 * @file
 * Helper functions to enforce email is king.
 */

/**
 * Validate a field to make sure the email is not a duplicate.
 */
function opencrm_unique_mail_form_validate($form, &$form_state) {
  foreach (element_children($form) as $key) {
    opencrm_unique_mail_form_validate($form[$key], $form_state);

    if (empty($form[$key]['#unique_email_validate'])) {
      continue;
    }

    $element = &$form[$key];
    $party = $element['#party'];

    // Get the value from form state.
    $value = drupal_array_get_nested_value($form_state['values'], $element['#parents']);
    $original = isset($element['#default_value']) ? $element['#default_value']: NULL;
    if (empty($value) || $value == $original) {
      continue;
    }

    // Get pids of parties with the same email.
    $query = db_select('party','p');
    $query->addField('p','pid');
    $query->addField('p','label');
    $query->condition('email', $value);
    if (!empty($party->pid)) {
      $query->condition('pid', $party->pid, '<>');
    }
    if (module_exists('party_hat')) {
      $hats = variable_get('party_user_registration_hats', array());

      if (!empty($hats)) {
        $query->leftJoin('field_data_party_hat', 'ph', "ph.entity_id = p.pid");
        $query->condition('ph.party_hat_hat_name', $hats);
      }
    }
    $other_parties = $query->execute()->fetchAllAssoc('pid');

    if (count($other_parties)) {
      $other_party = reset($other_parties);
      if (empty($element['#unique_email_message'])) {
        $element['#unique_email_message'] = t('A party with this email address already exists (!party_link).', array('!party_link' => l($other_party->label, entity_uri('party', party_load($other_party->pid)))));
      }
      form_set_error(implode('][', $element['#parents']), $element['#unique_email_message']);
    }
  }
}
