<?php
/**
 * @file
 * Export default flexiforms.
 */

/**
 * Implements hook_default_flexiform().
 */
function opencrm_default_flexiform() {
  $flexiforms = array();

  // Scan the directory for any exported indexes.
  $files = file_scan_directory(dirname(__FILE__) . '/flexiforms', '/\.json$/', array('key' => 'name'));
  foreach ($files as $file) {
    if ($form = entity_import('flexiform', file_get_contents($file->uri))) {
      $flexiforms[$form->form] = $form;
    }
  }

  return $flexiforms;
}
