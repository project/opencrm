<?php

/**
 * @file
 * Extended Views integration for Commerce.
 */

/**
 * Implements hook_views_data_alter().
 */
function opencrm_commerce_views_data_alter(&$data) {
  if (!empty($data['commerce_payment_transaction']['operations']['field']['handler'])) {
    $data['commerce_payment_transaction']['operations']['field']['handler'] = 'opencrm_commerce_views_handler_field_payment_transaction_operations';
  }
}

