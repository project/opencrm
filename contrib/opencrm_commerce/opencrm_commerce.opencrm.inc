<?php
/**
 * Open CRM hook implementations for OpenCRM Commerce.
 */

/**
 * Implements hook_opencrm_field_sync_fields().
 */
function opencrm_commerce_opencrm_field_sync_fields() {
  return array(
    'party_indiv_name' => array(
      'source' => 'party_indiv_name',
      'source entity_type' => 'profile2',
      'source bundle' => 'party_indiv',
      'bundles' => array(
        'commerce_customer_profile' => array_keys(commerce_customer_profile_types()),
      ),
    ),
    'commerce_customer_address' => array(
      'source' => 'party_indiv_address',
      'source entity_type' => 'profile2',
      'source bundle' => 'party_indiv',
    ),
  );
}

/**
 * Implements hook_opencrm_modal_pane_links().
 */
function opencrm_commerce_opencrm_modal_pane_links($context) {
  $links = array();

  if (drupal_match_path(current_path(), 'admin/booking/*')) {
    if (isset($context['pane']->type) && $context['pane']->type == 'entity_view') {
      if ($context['pane']->subtype == 'commerce_customer_profile') {
        $entity = $context['context'][$context['pane']->configuration['context']]->data;
        if (!empty($entity)) {
          $links['edit'] = array(
            'url' => 'admin/booking/' . $context['args'][0] . '/nojs/' . $context['pane']->subtype . '/' . $entity->profile_id,
            'text' => t('Edit'),
            'alt' => t('Edit @title', array('@title' => $context['pane']->title)),
          );
        }
      }
    }
    // Add 'add' links to commerce customer profile panes.
    elseif (isset($context['pane']->type) && $context['pane']->type == 'custom') {
      if (!empty($context['pane']->css['css_class'])) {
        $classes = explode(' ', $context['pane']->css['css_class']);
        if (in_array('create-customer-profile', $classes)) {
          if (in_array('commerce-customer-shipping', $classes)) {
            $field_name = 'commerce_customer_shipping';
          }
          elseif (in_array('commerce-customer-billing', $classes)) {
            $field_name = 'commerce_customer_billing';
          }
          if (isset($field_name)) {
            $links['add'] = array(
              'url' => 'admin/booking/' . $context['args'][0] . '/nojs/commerce_customer_profile/add/' . $field_name,
              'text' => t('Add'),
              'alt' => t('Add @title', array('@title' => $context['pane']->title)),
            );
          }
        }
      }
    }
  }

  return $links;
}
