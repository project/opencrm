<?php
/**
 * @file
 * Page callbacks for OpenCRM Commerce.
 */

/**
 * Page callback for adding an order.
 *
 * @param boolean $js
 *   Whether this is an AJAX request.
 * @param party $party
 *   The Party we are creating an order for.
 * @param string $order_type
 *   The type of order we want to create.
 */
function opencrm_commerce_order_add($js, $party, $order_type) {
  // Set the title.
  $info = entity_get_info('commerce_order');
  drupal_set_title(t('Add @order_type for @party', array(
    '@order_type' => $info['bundles'][$order_type]['label'],
    '@party' => $party->label,
  )));

  if ($js) {
    // This is a modal request, so do the magic.
    ctools_include('ajax');
    ctools_include('modal');

    $form_state = array(
      'ajax' => TRUE,
      'build_info' => array(
        'args' => array($party, $order_type),
      ),
    );
    $commands = ctools_modal_form_wrapper('opencrm_commerce_order_add_form', $form_state);

    if (!empty($form_state['executed'])) {
      // The form has been executed, so let's redirect to the destination page.
      $commands = array();
      $commands[] = ctools_ajax_command_redirect('admin/party/' . $party->pid . '/orders/' . $form_state['commerce_order']->order_id);
      $commands[] = ctools_modal_command_dismiss();
    }

    ajax_deliver(array(
      '#type' => 'ajax',
      '#commands' => $commands,
    ));
    drupal_exit();
  }
  else {
    return drupal_get_form('opencrm_commerce_order_add_form', $party, $order_type);
  }
}

/**
 * Form constructor for the OpenCRM Add Order form.
 *
 * @param Party $party
 *   The party we are creating an order for.
 * @param string $order_type
 *   The type of order we want to create.
 */
function opencrm_commerce_order_add_form($form, &$form_state, $party, $order_type) {
  // Create our order and set the party on it.
  $form['#order'] = commerce_order_new(0, NULL, $order_type);
  $order = entity_metadata_wrapper('commerce_order', $form['#order']);
  $order->party = $party;

  // Output the form.
  module_load_include('inc', 'commerce_order', 'includes/commerce_order.forms');
  $form = commerce_order_order_form($form, $form_state, $form['#order']);

  $hide = array(
    'commerce_line_items',
    'commerce_order_total',
    'party',
    'user',
    'order_history',
    'additional_settings',
  );
  foreach (element_children($form) as $key) {
    if (in_array($key, $hide)) {
      $form[$key]['#access'] = FALSE;
    }
  }

  unset($form['order_status']['#group']);
  $form['order_status']['#collapsible'] = FALSE;
  $form['order_status']['log']['#access'] = FALSE;

  $form['actions']['submit']['#submit'][] = 'opencrm_commerce_order_add_form_redirect';

  return $form;
}

/**
 * Submission handler for opencrm_commerce_order_add_form().
 */
function opencrm_commerce_order_add_form_redirect(&$form, &$form_state) {
  $order = entity_metadata_wrapper('commerce_order', $form_state['commerce_order']);
  $form_state['redirect'] = 'admin/party/' . $order->party->raw() . '/orders/' . $order->getIdentifier();
}

/**
 * Form API constructor for the edit order subset form.
 *
 * @param stdClass $order
 *   The order we are editing.
 * @param array $fields
 *   The field names we are exposing.
 * @param mixed $redirect
 *   A redirect path/array for the form.
 *
 * @see opencrm_commerce_edit_order_form_validate()
 * @see opencrm_commerce_edit_order_form_submit()
 */
function opencrm_commerce_edit_order_form($form, &$form_state, $order, $fields, $redirect = NULL) {
  $form['#order'] = $order;
  $form['#fields'] = $fields;
  $form['#redirect'] = $redirect;

  foreach ($fields as $field) {
    field_attach_form('commerce_order', $order, $form, $form_state, NULL, array('field_name' => $field));
  }

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

/**
 * Validation handler for opencrm_commerce_edit_order_form().
 *
 * @see opencrm_commerce_edit_order_form_submit()
 */
function opencrm_commerce_edit_order_form_validate($form, &$form_state) {
  foreach ($form['#fields'] as $field) {
    field_attach_form_validate('commerce_order', $form['#order'], $form, $form_state, array('field_name' => $field));
  }
}

/**
 * Submission handler for opencrm_commerce_edit_order_form().
 *
 * @see opencrm_commerce_edit_order_form_validate()
 */
function opencrm_commerce_edit_order_form_submit($form, &$form_state) {
  foreach ($form['#fields'] as $field) {
    field_attach_submit('commerce_order', $form['#order'], $form, $form_state, array('field_name' => $field));
  }
  commerce_order_save($form['#order']);
}

/**
 * Page callback for adding a customer profile to an order.
 *
 * @param boolean $js
 *   Whether this is an AJAX request.
 * @param stdClass $booking
 *   The booking we are adding the profile to.
 * @param string $field_name
 *   The field name of the customer profile field.
 */
function opencrm_commerce_customer_profile_add($js, $booking, $field_name) {
  // Set the title.
  $info = field_info_field($field_name);
  $profile = commerce_customer_profile_new($info['settings']['profile_type']);
  commerce_customer_profile_save($profile);
  $booking->{$field_name}[LANGUAGE_NONE][0]['profile_id'] = $profile->profile_id;
  commerce_order_save($booking);

  if ($js) {
    // This is a modal request, so do the magic.
    ctools_include('ajax');
    ctools_include('modal');

    $commands = array();
    $commands[] = ctools_ajax_command_redirect('admin/booking/' . $booking->order_id);
    $commands[] = ctools_modal_command_dismiss();

    ajax_deliver(array(
      '#type' => 'ajax',
      '#commands' => $commands,
    ));
    drupal_exit();
  }

  drupal_goto(current_path());
}
