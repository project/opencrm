<?php

/**
 * @file
 * Plugin to provide access control based on worklog permissions.
 */

$plugin = array(
  'title' => t('OpenCRM Commerce: Order belongs to party.'),
  'description' => t('Control access based on whether an order belongs to a party.'),
  'callback' => 'opencrm_commerce_order_belongs_to_party_ctools_access_check',
  'summary' => 'opencrm_commerce_order_belongs_to_party_ctools_access_summary',
  'required context' => array(
    new ctools_context_required(t('Order'), 'entity:commerce_order'),
    new ctools_context_required(t('Party'), 'entity:party'),
  ),
);

/**
 * Check access.
 */
function opencrm_commerce_order_belongs_to_party_ctools_access_check($conf, $contexts) {
  $order = array_shift($contexts)->data;
  $party = array_shift($contexts)->data;

  $wrapper = entity_metadata_wrapper('commerce_order', $order);
  $properties = $wrapper->getPropertyInfo();
  return !empty($properties['party']['field']) && ($wrapper->party->raw() == $party->pid);
}

/**
 * Summarise.
 */
function opencrm_commerce_order_belongs_to_party_ctools_access_summary() {
  return t('Order belongs to party.');
}
