<?php
/**
 * @file
 * CTools content for commerce order line items form.
 */

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'title' => t('Commerce order subset form'),
  'content_types' => 'opencrm_commerce_order_subset_form',
  // 'single' means not to be subtyped.
  'single' => TRUE,
  // Name of a function which will render the block.
  'render callback' => 'opencrm_commerce_order_subset_form_render',

  // Icon goes in the directory with the content type.
  'description' => t('Render a subset of an order.'),
  'required context' => new ctools_context_required(t('Commerce order'), 'entity:commerce_order'),
  'edit form' => 'opencrm_commerce_order_subset_form_form',
  'admin title' => 'opencrm_commerce_order_subset_form_title',

  // presents a block which is used in the preview of the data.
  // Pn Panels this is the preview pane shown on the panels building page.
  'category' => array(t('OpenCRM Commerce'), 0),
);

/**
 * Render the Party Attached Entity Edit Form
 *
 * @param $subtype
 * @param $conf
 *   Configuration as done at admin time
 * @param $args
 * @param $context
 *   Context - in this case we don't have any
 *
 * @return
 *   An object with at least title and content members
 */
function opencrm_commerce_order_subset_form_render($subtype, $conf, $args, $context) {
  $form_count = &drupal_static(__FUNCTION__, 0);
  $block = new stdClass();
  $block->title = NULL;

  $fields = $conf['fields'];
  if (!empty($conf['fields_exclude'])) {
    $bundle_fields = array_keys(field_info_instances('commerce_order', $context->data->type));
    $fields = array_diff($bundle_fields, $fields);
  }

  $form_state = array(
    'build_info' => array(
      'args' => array($context->data, $fields),
    ),
  );
  form_load_include($form_state, 'inc', 'opencrm_commerce', 'opencrm_commerce.pages');
  $block->content = drupal_build_form('opencrm_commerce_edit_order_form__' . $form_count++, $form_state);

  return $block;
}

/**
 * Config Form
 */
function opencrm_commerce_order_subset_form_form($form, &$form_state) {
  $form['fields'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Fields'),
    '#description' => t('Select the fields to show. If none are selected, all will be shown.'),
    '#default_value' => isset($form_state['conf']['fields']) ? $form_state['conf']['fields'] : NULL,
    '#options' => array(),
  );

  $order_info = entity_get_info('commerce_order');
  foreach (field_info_instances('commerce_order') as $instances) {
    foreach ($instances as $instance) {
      if (!isset($form['fields']['#options'][$instance['field_name']])) {
        $form['fields']['#options'][$instance['field_name']] = "<strong>{$instance['field_name']}</strong>: ";
      }
      else {
        $form['fields']['#options'][$instance['field_name']] .= ', ';
      }
      $form['fields']['#options'][$instance['field_name']] .= format_string('%label <small>[@bundle]</small>', array(
        '%label' => $instance['label'],
        '@bundle' => $order_info['bundles'][$instance['bundle']]['label'],
      ));
    }
  }

  $form['fields_exclude'] = array(
    '#type' => 'checkbox',
    '#title' => t('Exclude selected fields'),
    '#default_value' => isset($form_state['conf']['fields_exclude']) ? $form_state['conf']['fields_exclude'] : FALSE,
  );

  return $form;
}

function opencrm_commerce_order_subset_form_form_submit(&$form, &$form_state) {
  $form_state['conf']['fields'] = array_filter($form_state['values']['fields']);
  $form_state['conf']['fields_exclude'] = $form_state['values']['fields_exclude'];
}

/**
 * Title Callback
 */
function opencrm_commerce_order_subset_form_title($subtype, $conf, $context = NULL) {
  return t('Commerce order subset form');
}
