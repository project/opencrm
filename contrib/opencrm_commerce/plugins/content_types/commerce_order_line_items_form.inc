<?php
/**
 * @file
 * CTools content for commerce order line items form.
 */

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'title' => t('Commerce line items form'),
  'content_types' => 'commerce_order_line_items_form',
  // 'single' means not to be subtyped.
  'single' => TRUE,
  // Name of a function which will render the block.
  'render callback' => 'commerce_order_line_items_form_render',

  // Icon goes in the directory with the content type.
  'description' => t("Render a commerce order's line item form."),
  'required context' => new ctools_context_required(t('Commerce order'), 'entity:commerce_order'),
  'edit form' => 'commerce_order_line_items_form_form',
  'admin title' => 'commerce_order_line_items_form_title',

  // presents a block which is used in the preview of the data.
  // Pn Panels this is the preview pane shown on the panels building page.
  'category' => array(t('OpenCRM Commerce'), 0),
);

/**
 * Render the Party Attached Entity Edit Form
 *
 * @param $subtype
 * @param $conf
 *   Configuration as done at admin time
 * @param $args
 * @param $context
 *   Context - in this case we don't have any
 *
 * @return
 *   An object with at least title and content members
 */
function commerce_order_line_items_form_render($subtype, $conf, $args, $context) {
  $form_count = &drupal_static(__FUNCTION__, 0);
  $block = new stdClass();
  $block->title = t('Line items');

  $form_state = array(
    'build_info' => array(
      'args' => array($context->data, $conf),
    ),
  );
  form_load_include($form_state, 'inc', 'opencrm_commerce', 'plugins/content_types/commerce_order_line_items_form');
  $block->content = drupal_build_form('commerce_order_line_items_form_render_form__' . $form_count++, $form_state);

  return $block;
}

/**
 * Form constructor for the commerce order line item form.
 */
function commerce_order_line_items_form_render_form($form, &$form_state, $order, $conf = array()) {
  form_load_include($form_state, 'inc', 'opencrm_commerce', 'plugins/content_types/commerce_order_line_items_form');

  $form_state['commerce_order'] = $order;
  field_attach_form('commerce_order', $order, $form, $form_state, LANGUAGE_NONE, array('field_name' => 'commerce_line_items'));

  $form['commerce_line_items'][LANGUAGE_NONE]['#title'] = NULL;

  $form['actions'] = array(
    '#type' => 'actions',
  );
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save changes'),
  );

  if (!empty($conf['line_item_types'])) {
    // Hide line items we don't want to deal with.
    foreach ($form['commerce_line_items'][LANGUAGE_NONE]['line_items'] as $key => &$element) {
      if (empty($conf['line_item_types_exclude']) xor in_array($element['line_item']['#value']->type, $conf['line_item_types'], TRUE)) {
        $element['#access'] = FALSE;
      }
    }

    // Hide line items from the add form.
    foreach (array_keys($form['commerce_line_items'][LANGUAGE_NONE]['actions']['line_item_type']['#options']) as $type) {
      if (empty($conf['line_item_types_exclude']) xor in_array($type, $conf['line_item_types'], TRUE)) {
        unset($form['commerce_line_items'][LANGUAGE_NONE]['actions']['line_item_type']['#options'][$type]);
      }
    }

    // Simplify single items.
    $type_count = count($form['commerce_line_items'][LANGUAGE_NONE]['actions']['line_item_type']['#options']);
    if ($type_count == 0) {
      $form['commerce_line_items'][LANGUAGE_NONE]['actions']['actions']['#access'] = FALSE;
    }
    // @TODO: Make the below work.
    /*elseif ($type_count == 1) {
      $label = reset($form['commerce_line_items'][LANGUAGE_NONE]['actions']['line_item_type']['#options']);
      $form['commerce_line_items'][LANGUAGE_NONE]['actions']['line_item_add']['#value'] = t('Add @type', array('@type' => $label));
      $form['commerce_line_items'][LANGUAGE_NONE]['actions']['line_item_add']['#prefix'] = $form['commerce_line_items'][LANGUAGE_NONE]['actions']['line_item_type']['#prefix'];
      $form['commerce_line_items'][LANGUAGE_NONE]['actions']['line_item_type'] = array(
        '#type' => 'value',
        '#value' => reset(array_keys($form['commerce_line_items'][LANGUAGE_NONE]['actions']['line_item_type']['#options'])),
      );

    }*/
  }

  return $form;
}

/**
 * Validation callback for commerce_order_line_items_form_render_form().
 */
function commerce_order_line_items_form_render_form_validate(&$form, &$form_state) {
  field_attach_form_validate('commerce_order', $form_state['commerce_order'], $form, $form_state);
}

/**
 * Submission callback for commerce_order_line_items_form_render_form().
 */
function commerce_order_line_items_form_render_form_submit(&$form, &$form_state) {
  field_attach_submit('commerce_order', $form_state['commerce_order'], $form, $form_state);
  commerce_order_save($form_state['commerce_order']);
}

/**
 * Config Form
 */
function commerce_order_line_items_form_form($form, &$form_state) {
  $form['conf']['line_item_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Line item types'),
    '#description' => t('Select the line item types to show. If none are selected, all will be shown.'),
    '#default_value' => isset($form_state['conf']['line_item_types']) ? $form_state['conf']['line_item_types'] : NULL,
    '#options' => array(),
  );
  foreach (commerce_line_item_types() as $type) {
    $form['conf']['line_item_types']['#options'][$type['type']] = $type['name'];
  }

  $form['conf']['line_item_types_exclude'] = array(
    '#type' => 'checkbox',
    '#title' => t('Exclude selected line items'),
    '#default_value' => isset($form_state['conf']['line_item_types_exclude']) ? $form_state['conf']['line_item_types_exclude'] : FALSE,
  );

  return $form;
}

function commerce_order_line_items_form_form_submit(&$form, &$form_state) {
  foreach (element_children($form['conf']) as $key) {
    if (isset($form_state['values'][$key])) {
      $form_state['conf'][$key] = $form_state['values'][$key];
    }
  }
}

/**
 * Title Callback
 */
function commerce_order_line_items_form_title($subtype, $conf, $context = NULL) {
  return t('Commerce line items form');
}
