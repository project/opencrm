<?php

/**
 * @file
 * Plugin to provide an relationship handler for term from node.
 */

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file. Only define if the modules exist.
 */
if (module_exists('commerce_order') && module_exists('commerce_customer')) {
  $plugin = array(
    'title' => t('Commerce Customer Profile from Commerce Order'),
    'keyword' => 'customer_profile',
    'description' => t('Adds a customer profile from an Order entity'),
    'required context' => new ctools_context_required(t('Order'), 'entity:commerce_order'),
    'context' => 'commerce_customer_profile_from_order_context',
    'edit form' => 'commerce_customer_profile_from_order_settings_form',
    'defaults' => array('type' => 'billing'),
  );
}

/**
 * Return a new context based on an existing context.
 */
function commerce_customer_profile_from_order_context($context, $conf) {
  $type = $conf['type'];

  // If unset it wants a generic, unfilled context, which is just NULL.
  if (empty($context->data)) {
    return ctools_context_create_empty('entity:commerce_customer_profile', NULL);
  }

  $order = $context->data;
  $wrapper = entity_metadata_wrapper('commerce_order', $order);
  $profile = NULL;

  // If the associated order field has been set...
  if ($field_name = variable_get('commerce_customer_profile_' . $type . '_field', '')) {
    $profile = $wrapper->{$field_name}->value();
  }
  else {
    // Or try the association stored in the order's data array if no field is set.
    if (!empty($order->data['profiles']['customer_profile_' . $type])) {
      $profile = commerce_customer_profile_load($order->data['profiles']['customer_profile_' . $type]);
    }
  }

  if (empty($profile)) {
    return ctools_context_create_empty('entity:commerce_customer_profile', NULL);
  }

  $profile->entity_context = array(
    'entity_type' => 'commerce_order',
    'entity_id' => $order->order_id,
  );

  return ctools_context_create('entity:commerce_customer_profile', $profile);
}

/**
 * Settings form for the relationship.
 */
function commerce_customer_profile_from_order_settings_form($form, &$form_state) {
  $conf = $form_state['conf'];

  $options = array();
  $info = entity_get_info('commerce_customer_profile');
  foreach ($info['bundles'] as $bundle => $settings) {
    $options[$bundle] = $settings['label'];
  }
  $form['type'] = array(
    '#title' => t('Type'),
    '#type' => 'select',
    '#options' => $options,
    '#default_value' => $conf['type'],
    '#prefix' => '<div class="clearfix">',
    '#suffix' => '</div>',
  );

  return $form;
}
