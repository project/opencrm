<?php
/**
 * @file
 * Provides tokens not available in core commerce.
 */

/**
 * Implements hook_token_info();
 */
function opencrm_commerce_token_info() {
  // Tokens
  $order['balance'] = array(
    'name' => t('Balance'),
    'description' => t('The remaining balance on the order'),
  );

  $order['paid-so-far'] = array(
    'name' => t('Amount Paid'),
    'description' => t('The Amount Paid towards this Order'),
  );

  $order['total'] = array(
    'name' => t('Total'),
    'description' => t('The total cost of the order'),
  );

  return array(
    'tokens' => array('commerce-order' => $order),
  );
}

/**
 * Implements hook_tokens().
 */
function opencrm_commerce_tokens($type, $tokens, array $data = array(), array $options = array()) {
  $replacements = array();

  if ($type != 'commerce-order' || empty($data['commerce-order'])) {
    return $replacements;
  }
  $order = $data['commerce-order'];
  $order_wrapper = entity_metadata_wrapper('commerce_order', $order);

  foreach ($tokens as $token) {
    if (!in_array($token, array('balance', 'paid-so-far', 'total'))) {
      continue;
    }

    switch ($token) {
      case 'balance':
        $value = commerce_payment_order_balance($order);
        break;
      case 'total':
        $value = $order_wrapper->commerce_order_total->value();
        break;
      case 'paid-so-far':
        // Taken from commerce_payment_order_balance
        $order_total = $order_wrapper->commerce_order_total->value();
        $transaction_statuses = commerce_payment_transaction_statuses();
        $totals = array();

        foreach (commerce_payment_transaction_load_multiple(array(), array('order_id' => $order->order_id)) as $transaction) {
          // If the payment transaction status indicates it should include the
          // current transaction in the total...
          if ($transaction_statuses[$transaction->status]['total']) {
            // Add the transaction to its currency's running total if it exists...
            if (isset($totals[$transaction->currency_code])) {
              $totals[$transaction->currency_code] += $transaction->amount;
            }
            else {
              // Or begin a new running total for the currency.
              $totals[$transaction->currency_code] = $transaction->amount;
            }
          }
        }

        // Only return a balance if the totals array contains a single matching currency.
        if (count($totals) == 1 && isset($totals[$order_total['currency_code']])) {
          $value = array('amount' => $totals[$order_total['currency_code']], 'currency_code' => $order_total['currency_code']);
        }
        elseif (empty($totals)) {
          $value = array('amount' => 0, 'currency_code' => $order_total['currency_code']);
        }
        else {
          return array();
        }
        break;
    }

    $replacements[$token] = commerce_currency_format($value['amount'], $value['currency_code']);
  }

  return $replacements;
}
