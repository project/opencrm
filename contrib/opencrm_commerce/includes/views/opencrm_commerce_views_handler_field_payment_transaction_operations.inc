<?php

/**
 * Field handler to present a payment transaction's operations links.
 */
class opencrm_commerce_views_handler_field_payment_transaction_operations extends views_handler_field {
  function option_definition() {
    $options = parent::option_definition();
    $options['use_modal'] = array('default' => FALSE);

    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['use_modal'] = array(
      '#type' => 'checkbox',
      '#title' => t('Load operations in modal popups'),
      '#default_value' => $this->options['use_modal'],
    );
  }

  function construct() {
    parent::construct();

    $this->additional_fields['transaction_id'] = 'transaction_id';
    $this->additional_fields['order_id'] = 'order_id';
  }

  function query() {
    $this->ensure_my_table();
    $this->add_additional_fields();
  }

  function render($values) {
    $transaction_id = $this->get_value($values, 'transaction_id');
    $order_id = $this->get_value($values, 'order_id');

    $path_start = empty($this->options['use_modal']) ? 'admin/' : 'admin/modal/nojs/';
    $links = menu_contextual_links('commerce-payment-transaction', $path_start.'commerce/orders/' . $order_id . '/payment', array($transaction_id));

    if (!empty($links)) {
      if (!empty($this->options['use_modal'])) {
        ctools_include('ajax');
        ctools_include('modal');
        ctools_modal_add_js();

        foreach ($links as &$link) {
          $link['attributes']['class'][] = 'ctools-use-modal';
        }
      }
      drupal_add_css(drupal_get_path('module', 'commerce_payment') . '/theme/commerce_payment.admin.css');
      return theme('links', array('links' => $links, 'attributes' => array('class' => array('links', 'inline', 'operations'))));
    }
  }
}
