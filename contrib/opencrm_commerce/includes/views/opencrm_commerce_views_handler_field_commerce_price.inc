<?php
/**
 * @file
 *   Views handler for commerce price properties.
 */

class opencrm_commerce_views_handler_field_commerce_price extends entity_views_handler_field_numeric {

  /**
   * Provide options for this handler.
   */
  public function option_definition() {
    $options = parent::option_definition();

    // Get rid of unwanted numberic options.
    unset($options['set_precision']);
    unset($options['precision']);
    unset($options['decimal']);
    unset($options['separator']);
    unset($options['format_plural']);
    unset($options['format_plural_singular']);
    unset($options['format_plural_plural']);
    unset($options['prefix']);
    unset($options['suffix']);

    // Add options for price.
    $options['price_format'] = array(
      'default' => 'formatted_amount',
    );

    return $options;
  }

  /**
   * Provide a options form for this handler.
   */
  public function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    unset($form['separator']);
    unset($form['format_plural']);
    unset($form['format_plural_singular']);
    unset($form['format_plural_plural']);
    unset($form['prefix']);
    unset($form['suffix']);

    $form['price_format'] = array(
      '#type' => 'select',
      '#title' => t('Format'),
      '#description' => t('Select the price format.'),
      '#options' => array(
        'raw_amount' => t('Raw Amount'),
        'formatted_amount' => t('Formatted Amount'),
        'formatted_components' => t('Formatted Amount with Components'),
      ),
      '#default_value' => $this->options['price_format'],
      '#weight' => -1,
    );
  }

  /**
   * Render the field.
   *
   * @param $values
   *   The values retrieved from the database.
   */
  public function render($values) {
    $value = $this->get_value($values);
    if (isset($value['amount'])) {
      switch ($this->options['price_format']) {
        case 'raw_amount':
          return check_plain($value['amount']);
        case 'formatted_amount':
          return commerce_currency_format(
            $value['amount'],
            $value['currency_code'],
            EntityFieldHandlerHelper::get_value($this, $values, 'entity object')
          );
        case 'formatted_components':
          $item = $value;
          $entity = EntityFieldHandlerHelper::get_value($this, $values, 'entity object');

          // Build an array of component display titles and their prices.
          $components = array();
          $weight = 0;

          foreach ($item['data']['components'] as $key => $component) {
            $component_type = commerce_price_component_type_load($component['name']);

            if (empty($components[$component['name']])) {
              $components[$component['name']] = array(
                'title' => check_plain($component_type['display_title']),
                'price' => commerce_price_component_total($item, $component['name']),
                'weight' => $component_type['weight'],
              );

              $weight = max($weight, $component_type['weight']);
            }
          }

          // If there is only a single component and its price equals the field's,
          // then remove it and just show the actual price amount.
          if (count($components) == 1 && in_array('base_price', array_keys($components))) {
            $components = array();
          }

          // Add the actual field value to the array.
          $components['commerce_price_formatted_amount'] = array(
            'title' => check_plain($translated_instance['label']),
            'price' => $item,
            'weight' => $weight + 1,
          );

          // Allow other modules to alter the components.
          drupal_alter('commerce_price_formatted_components', $components, $item, $entity);

          // Sort the components by weight.
          uasort($components, 'drupal_sort_weight');

          // Format the prices for display.
          foreach ($components as $key => &$component) {
            $component['formatted_price'] = commerce_currency_format(
              $component['price']['amount'],
              $component['price']['currency_code'],
              $entity
            );
          }

          return theme('commerce_price_formatted_components', array('components' => $components, 'price' => $item));
      }
    }
  }
}
