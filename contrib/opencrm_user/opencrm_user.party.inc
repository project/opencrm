<?php
/**
 * @file
 * Party general hooks include.
 */

/**
 * Implements hook_party_access().
 */
function opencrm_user_party_access($op, $party, $data_set, $account) {
  global $user;

  // If the account passed is null, or the accont passed is the anonymous user
  // AND is the user that is logged in then get the opencrm current user.
  if (is_null($account) || ($user->uid == 0 && $user->uid == $account->uid)) {
    $opencrm_account = opencrm_user_current();
    // If the opencrm_current_user is not the same as the currently loffed in
    // user then run access checks for the opencrm current user.
    if ($opencrm_account->uid != $user->uid) {
      foreach (module_implements('party_access') as $module) {
        if ($module == 'opencrm_user') {
          continue;
        }

        $access[$module] = module_invoke($module, 'party_access', $op, $party, $data_set, $opencrm_account);
      }

      if (in_array(FALSE, $access, TRUE)) {
        return FALSE;
      }
      elseif (in_array(TRUE, $access, TRUE)) {
        return TRUE;
      }
    }
  }
}
