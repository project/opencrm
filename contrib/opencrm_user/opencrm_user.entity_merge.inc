<?php
/**
 * Entity merge hook implementations.
 */

/**
 * Implements hook_entity_merge_register_processes().
 */
function opencrm_user_entity_merge_register_processes($entity_type, $entity1, $entity2, $conf) {
  // We only working with parties.
  if ($entity_type != 'party') {
    return array();
  }

  /* @var Party $entity1 */
  /* @var Party $entity2 */

  // Get hold of the web accounts on the users.
  $user1 = $entity1->getDataSetController('user')->getEntity(0);
  $user2 = $entity2->getDataSetController('user')->getEntity(0);

  // If there is only a user on party 2, we'll simply move it.
  if (!$user1 && $user2) {
    $processes['opencrm_user_entityreference'] = array(
      'class' => 'OpenCrmMergeProcessMoveWebAccount',
      'weight' => 5,
    );
  }
  // Otherwise, if there aren't 2 users, there are no processes to register.
  if (!$user1 || !$user2) {
    return array();
  }

  // Register any requested processes.
  $processes = array();

  // If we are updating any references to a user, register the process.
  if (module_exists('entityreference') && !empty($conf['opencrm_user']['entityreference'])) {
    $processes['opencrm_user_entityreference'] = array(
      'class' => 'EntityMergeProcessEntityReference',
      'entity1' => $user1,
      'entity2' => $user2,
      'targetEntity' => $user1,
      'weight' => 5,
    );
  }

  // If we are merging roles, register the process.
  if (!empty($conf['opencrm_user']['roles'])) {
    $processes['opencrm_user_roles'] = array(
      'class' => 'OpenCrmMergeProcessMergeRoles',
      'entity1' => $user1,
      'entity2' => $user2,
      'targetEntity' => $user1,
      'weight' => 5,
    );
    $processes['opencrm_user_save_account'] = array(
      'class' => 'EntityMergeProcessSaveTarget',
      'entity1' => $user1,
      'entity2' => $user2,
      'targetEntity' => $user1,
      'weight' => 6,
    );
  }

  // If we have a method for this merge, add the relevant processes.
  if (!empty($conf['opencrm_user']['method'])) {
    $processes['opencrm_user_remove_account'] = array(
      'class' => 'OpenCrmMergeProcessRemoveWebAccount',
      'entity1' => $user1,
      'entity2' => $user2,
      'targetEntity' => $user1,
      'weight' => 6,
    );
  }

  return $processes;
}
