<?php
/**
 * @file
 * Contains the User Registration Attachment flexiform display class.
 */

/**
 * Allow forms to be attached to the user registration process.
 */
class OpencrmUserDisplayUserRegistration extends FlexiformDisplayPageBase {

  /**
   * {@inheritdoc}
   */
  public function hook_menu() {
    $items =  parent::hook_menu();

    foreach ($items as &$item) {
      $item['access callback'] = 'opencrm_user_flexiform_user_registration_access';

      // Unset page arguments if no permission is provided.
      if (empty($this->configuration['access']['permission'])) {
        unset($items[$this->configuration['path']]['access arguments']);
      }
    }

    return $items;
  }

  /**
   * {@inheritdoc}
   */
  public function getBaseEntity($context = array()) {
    return opencrm_user_current();
  }

}
