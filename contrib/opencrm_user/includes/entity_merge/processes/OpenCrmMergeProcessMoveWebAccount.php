<?php

/**
 * @file
 * Contains OpenCrmMergeProcessMoveWebAccount
 */

/**
 * Merge process class to move a web account from party 2 to the target party.
 */
class OpenCrmMergeProcessMoveWebAccount extends EntityMergeProcessBase {

  /**
   * {@inheritdoc}
   */
  public function run() {
    // Detach the user from party 2.
    $controller2 = $this->entity2->getDataSetController('user');
    $user = $controller2->detachEntityByDelta(0, TRUE);
    $controller2->save();

    // Attach it to party 1.
    $this->targetEntity->getDataSetController('user')->attachEntity($user)->save();
  }

}
