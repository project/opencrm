<?php

/**
 * @file
 * Contains OpenCrmMergeProcessRemoveWebAccount
 */

/**
 * Merge process class to remove a web account, either by blocking or deleting.
 */
class OpenCrmMergeProcessRemoveWebAccount extends EntityMergeProcessBase {

  /**
   * {@inheritdoc}
   */
  public function run() {
    $method = !empty($this->conf['opencrm_user']['method']) ? $this->conf['opencrm_user']['method'] : NULL;
    switch ($method) {
      case 'delete':
        entity_delete('user', $this->entity2->uid);
        break;
      case 'block':
        $this->entity2->status = 0;
        entity_save('user', $this->entity2);
        break;
    }
  }

}
