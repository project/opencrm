<?php

/**
 * @file
 * Contains OpenCrmMergeProcessMergeRoles
 */

/**
 * Merge process class to merge roles from one user to another.
 */
class OpenCrmMergeProcessMergeRoles extends EntityMergeProcessBase {

  /**
   * {@inheritdoc}
   */
  public function run() {
    foreach ($this->entity1->roles as $rid => $name) {
      if (!isset($this->targetEntity->roles[$rid])) {
        $this->targetEntity->roles[$rid] = $name;
      }
    }
    foreach ($this->entity2->roles as $rid => $name) {
      if (!isset($this->targetEntity->roles[$rid])) {
        $this->targetEntity->roles[$rid] = $name;
      }
    }
  }

}
