<?php
/**
 * @file
 * OpenCRM User implementation of flexiform hooks.
 */

/**
 * Implements hook_flexiform_display_info().
 */
function opencrm_user_flexiform_display_info() {
  $displays = array();

  $displays['OpencrmUserDisplayUserRegistration'] = array(
    'label' => t('User Registration Attachment'),
    'description' => t('Attach the form to the user registration process. The user will need to register before completing the form and will be directed through.'),
  );

  return $displays;
}
