<?php
/**
 * @file
 * OpenCRM hook implementations.
 */

/**
 * Implements hook_opencrm_dedupe_verify_property_info_alter().
 */
function opencrm_user_opencrm_dedupe_verify_property_info_alter(&$info) {
  $info[t('Web Account')]['user:mail']['compare'] = 'opencrm_user_dedupe_compare_account';
  $info[t('Web Account')]['user:roles'] = array(
    'label' => t('Roles'),
    'use label' => TRUE,
    'compare' => 'opencrm_user_dedupe_compare_roles',
  );
}

/**
 * Callback for comparing web accounts.
 *
 * @see callback_opencrm_dedupe_compare()
 */
function opencrm_user_dedupe_compare_account($v1, $v2, &$prevent_merge, &$form) {
  if ($v1->raw() !== NULL && $v2->raw() !== NULL) {
    $form['opencrm_user'] = array(
      '#type' => 'fieldset',
      '#title' => t('Web Account'),
    );

    $form['opencrm_user']['method'] = array(
      '#type' => 'radios',
      '#title' => t("What should we do with the duplicate's web account?"),
      '#options' => array(
        'block' => t('Block it'),
        'delete' => t('Delete it'),
      ),
      '#default_value' => 'block',
      '#required' => TRUE,
    );

    $form['opencrm_user']['entityreference'] = array(
      '#type' => 'checkbox',
      '#title' => t('Attempt to move all references onto to the primary web account'),
      '#description' => t('Note this will only merge references we are aware of.'),
      '#default_value' => TRUE,
    );

    return FALSE;
  }
  elseif ($v2->raw() !== NULL) {
    return TRUE;
  }
}

/**
 * Callback for comparing web accounts roles.
 *
 * @see callback_opencrm_dedupe_compare()
 */
function opencrm_user_dedupe_compare_roles($v1, $v2, &$prevent_merge, &$form) {
  if ($v1->raw() !== NULL && $v2->raw() !== NULL && $roles = array_diff($v2->roles->raw(), $v1->roles->raw())) {
    $role_labels = array();
    foreach ($v2->roles as $role) {
      if (in_array($role->value(), $roles)) {
        $role_labels[] = $role->label();
      }
    }
    $form['opencrm_user']['roles'] = array(
      '#type' => 'checkbox',
      '#title' => t('Give these roles to primary web account: %roles', array(
        '%roles' => implode(', ', $role_labels),
      )),
      '#default_value' => FALSE,
    );

    return FALSE;
  }
}
