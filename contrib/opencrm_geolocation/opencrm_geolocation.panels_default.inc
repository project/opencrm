<?php
/**
 * @file
 * opencrm_geolocation.panels_default.inc
 */

/**
 * Implements hook_default_panels_mini_alter().
 *
 * Override the default party dashboard summary and party view mini panels.
 */
function opencrm_geolocation_default_panels_mini_alter(&$export) {
  if (isset($export['party_view__summary'])) {
    $display = $export['party_view__summary']->display;

    $pane = new stdClass();
    $pane->pid = 'geolocation';
    $pane->panel = 'right';
    $pane->type = 'party_geo_override_form';
    $pane->subtype = 'party_geo_override_form';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'context' => 'requiredcontext_entity:party_1',
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'dashboard-block',
    );
    $pane->extras = array();
    $pane->position = max($display->panels[$pane->panel]) + 1;
    $pane->locks = array();

    $display->content[$pane->pid] = $pane;
    $display->panels[$pane->panel][$pane->position] = $pane->pid;
  }
}
