<?php

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'single' => TRUE,
  'title' => t('Party geolocation override form'),
  'description' => t('Form to override geolocation fields on the party.'),
  'required context' => new ctools_context_required(t('Party'), 'entity:party'),
  'category' => t('Party Dashboard'),
);

function opencrm_geolocation_party_geo_override_form_content_type_render($subtype, $conf, $panel_args, &$context) {
  $block = new stdClass();
  $block->title = t('Geolocation');

  $form_state = array();
  $form_state['build_info']['args'] = array($context->data);
  form_load_include($form_state, 'inc', 'opencrm_geolocation', 'plugins/content_types/party_geo_override_form.pages');
  $block->content = drupal_build_form('opencrm_geolocation_party_geo_override_form', $form_state);

  return $block;
}

function opencrm_geolocation_party_geo_override_form_content_type_admin_title($subtype, $conf, $context) {
  return t('Party geolocation override form');
}

function opencrm_geolocation_party_geo_override_form_content_type_edit_form($form, &$form_state) {
  // provide a blank form so we have a place to have context setting.
  return $form;
}

/**
 * Form API constructor for the party geolocation override form.
 *
 * @see opencrm_geolocation_party_geo_override_form_validate()
 * @see opencrm_geolocation_party_geo_override_form_submit()
 */
function opencrm_geolocation_party_geo_override_form($form, &$form_state, $party) {
  $form['#party'] = $party;

  field_attach_form('party', $party, $form, $form_state, NULL, array('field_name' => 'party_geolocation_override'));
  $form['party_geolocation_override'][LANGUAGE_NONE][0]['geom']['#title'] = NULL;

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

/**
 * Form validation handler for opencrm_geolocation_party_geo_override_form().
 *
 * @see opencrm_geolocation_party_geo_override_form_submit()
 */
function opencrm_geolocation_party_geo_override_form_validate(&$form, &$form_state) {
  field_attach_form_validate('party', $form['#entity'], $form, $form_state, array('field_name' => 'party_geolocation_override'));
}

/**
 * Form submission handler for opencrm_geolocation_party_geo_override_form().
 *
 * @see opencrm_geolocation_party_geo_override_form_validate()
 */
function opencrm_geolocation_party_geo_override_form_submit(&$form, &$form_state) {
  field_attach_submit('party', $form['#party'], $form, $form_state, array('field_name' => 'party_geolocation_override'));
  $form['#party']->save();
}
