<?php
/**
 * @file
 * Install/update hooks for OpenCRM Geolocation.
 */

/**
 * Implements hook_schema().
 */
function opencrm_geolocation_schema() {
  $schema['opencrm_geolocation_fails'] = array(
    'description' => 'Track geolocation failures so we don\'t process them again.',
    'fields' => array(
      'field' => array(
        'description' => 'The field that failed.',
        'type' => 'varchar',
        'length' => 32,
        'not null' => TRUE,
      ),
      'entity_type' => array(
        'description' => 'The entity type that failed.',
        'type' => 'varchar',
        'length' => 128,
        'not null' => TRUE,
      ),
      'entity_id' => array(
        'description' => 'The entity id that has failed.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
    ),
    'primary key' => array('field', 'entity_type', 'entity_id'),
  );
  return $schema;
}

/**
 * Implements hook_install().
 */
function opencrm_geolocation_install() {
  $t = get_t();

  // Add the geofields to individuals and organisations.
  if (!field_info_field('party_geolocation')) {
    $field = array(
      'field_name' => 'party_geolocation',
      'type' => 'geofield',
      'cardinality' => 1,
    );
    field_create_field($field);
  }

  $default_instance = array(
    'entity_type' => 'profile2',
    // Bundle set when creating instance.
    'field_name' => 'party_geolocation',
    'label' => $t('Geolocation'),
    'widget' => array(
      'type' => 'geocoder',
      'settings' => array(
        // geocoder_field set when creating instance.
        'geocoder_handler' => 'google',
        'handler_settings' => array(
          'google' => array(
            'geometry_type' => 'point',
            'all_results' => 0,
          ),
        ),
      ),
    ),
    'display' => array(
      'default' => array(
        'type' => 'hidden',
      ),
    ),
  );
  if (!field_info_instance('profile2', 'party_geolocation', 'party_indiv')) {
    $instance = $default_instance;
    $instance['bundle'] = 'party_indiv';
    $instance['widget']['settings']['geocoder_field'] = 'party_indiv_address';
    field_create_instance($instance);
  }
  if (!field_info_instance('profile2', 'party_geolocation', 'party_org')) {
    $instance = $default_instance;
    $instance['bundle'] = 'party_org';
    $instance['widget']['settings']['geocoder_field'] = 'party_org_address';
    field_create_instance($instance);
  }

  // The geofield cache on the party is a little different.
  if (!field_info_instance('party', 'party_geolocation', 'party')) {
    $instance = array(
      'entity_type' => 'party',
      'bundle' => 'party',
      'field_name' => 'party_geolocation',
      'label' => $t('Geolocation'),
      'widget' => array(
        'type' => 'party_primary_field',
        'settings' => array(
          'sources' => array(
            'party:party_geolocation_override' => array(
              'data_set' => 'party',
              'property' => 'party_geolocation_override',
              'value' => '',
              'weight' => '0',
              'callback' => '',
            ),
            'profile2_party_indiv:party_geolocation' => array(
              'data_set' => 'profile2_party_indiv',
              'property' => 'party_geolocation',
              'value' => '',
              'weight' => '1',
              'callback' => '',
            ),
            'profile2_party_org:party_geolocation' => array(
              'data_set' => 'profile2_party_org',
              'property' => 'party_geolocation',
              'value' => '',
              'weight' => '2',
              'callback' => '',
            ),
          ),
        ),
      ),
      'display' => array(
        'default' => array(
          'type' => 'hidden',
        ),
      ),
    );
    field_create_instance($instance);
  }

  if (!field_info_field('party_geolocation_override')) {
    $field = array(
      'field_name' => 'party_geolocation_override',
      'type' => 'geofield',
      'cardinality' => 1,
    );
    field_create_field($field);
  }

  if (!field_info_instance('party', 'party_geolocation_override', 'party')) {
    $instance = array(
      'field_name' => 'party_geolocation_override',
      'entity_type' => 'party',
      'bundle' => 'party',
      'label' => $t('Geolocation override'),
      'widget' => array(
        'type' => 'geofield_latlon',
      ),
    );
    field_create_instance($instance);
  }
}

/**
 * Implements hook_uninstall().
 */
function opencrm_geolocation_uninstall() {
  // Remove the fields we created.
  field_delete_field('party_geolocation');
  field_delete_field('party_geolocation_override');
}
