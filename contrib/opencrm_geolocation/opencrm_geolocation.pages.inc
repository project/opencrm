<?php
/**
 * @file
 * Page callbacks for OpenCRM Geolocation.
 */

/**
 * Form API constructor for the Geolocation configuration form.
 *
 * @see opencrm_geolocation_config_form_validate()
 * @see opencrm_geolocation_config_form_submit()
 */
function opencrm_geolocation_config_form($form, &$form_state) {
  $form['opencrm_geolocation_cron'] = array(
    '#type' => 'checkbox',
    '#title' => t('Geocode on cron'),
    '#description' => t('If enabled, we will attempt to generate geolocations for parties on cron.'),
    '#default_value' => variable_get('opencrm_geolocation_cron'),
  );

  $form['opencrm_geolocation_process_limit'] = array(
    '#type' => 'textfield',
    '#title' => t('Maximum geocode requests per day'),
    '#default_value' => variable_get('opencrm_geolocation_process_limit', 1500),
    '#description' => t('Set a limit on the maximum number of geocoding requests per day. This only affects the process based geocoding, not saving an entity.')
  );

  $queue = DrupalQueue::get('opencrm_geolocation_cron_process')->numberOfItems() > 0;
  if ($queue) {
    $form['queue_clear'] = array(
      '#type' => 'checkbox',
      '#title' => t('Clear the active queue on save.'),
    );
  }
  else {
    $limits = variable_get('opencrm_geolocation_cron_process', array());
    $form['queue_start'] = array(
      '#type' => 'checkbox',
      '#title' => t('Queue a new geocoding process on save.'),
    );
    if (!empty($limits['day'])) {
      $form['queue_start']['#description'] = t('The queue last ran %date.', array(
        '%date' => date('j M y', strtotime($limits['day'])),
      ));
    }
  }

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

/**
 * Form validation handler for opencrm_geolocation_config_form().
 *
 * @see opencrm_geolocation_config_form()
 * @see opencrm_geolocation_config_form_submit()
 */
function opencrm_geolocation_config_form_validate(&$form, &$form_state) {
  if (!is_numeric($form_state['values']['opencrm_geolocation_process_limit'])) {
    form_error($form['opencrm_geolocation_process_limit'], t('Please enter a valid number for the daily request limit.'));
  }
}

/**
 * Form submission handler for opencrm_geolocation_config_form().
 *
 * @see opencrm_geolocation_config_form()
 * @see opencrm_geolocation_config_form_validate()
 */
function opencrm_geolocation_config_form_submit(&$form, &$form_state) {
  variable_set('opencrm_geolocation_cron', !empty($form_state['values']['opencrm_geolocation_cron']));
  variable_set('opencrm_geolocation_process_limit', $form_state['values']['opencrm_geolocation_process_limit']);

  $queue = DrupalQueue::get('opencrm_geolocation_cron_process');
  if (!empty($form_state['values']['queue_clear'])) {
    $queue->deleteQueue();
  }
  else {
    $queue->createItem(array());
  }

  drupal_set_message(t('Geolocation settings successfully saved.'));
}
