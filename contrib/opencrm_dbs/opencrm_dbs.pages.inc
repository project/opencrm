<?php
/**
 * @file
 * Page callbacks for OpenCRM DBS.
 */

/**
 * Form API constructor for the DBS configuration form.
 *
 * @see opencrm_dbs_config_form_validate()
 * @see opencrm_dbs_config_form_submit()
 */
function opencrm_dbs_config_form($form, &$form_state) {
  $expiry = variable_get('opencrm_dbs_expiry');
  $use_workforces = variable_get('opencrm_dbs_use_workforces');
  $form['opencrm_dbs_expiry'] = array(
    '#type' => 'textfield',
    '#title' => t('DBS expires after'),
    '#description' => t('Enter a duration a DBS is valid for, e.g. %years or %months. The current value of %current would mean a DBS confirmed today would expire %expires.', array(
      '%years' => '5 years',
      '%months' => '18 months',
      '%current' => $expiry,
      '%expires' => format_date(strtotime('+' . $expiry)),
    )),
    '#size' => 20,
    '#default_value' => $expiry,
  );

  $form['opencrm_dbs_use_workforces'] = array(
    '#type' => 'checkbox',
    '#title' => t('DBS Workforces'),
    '#description' => t('Enable the use of workforces separate workforce types.'),
    '#default_value' => $use_workforces,
  );


  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

/**
 * Form validation handler for opencrm_dbs_config_form().
 *
 * @see opencrm_dbs_config_form()
 * @see opencrm_dbs_config_form_submit()
 */
function opencrm_dbs_config_form_validate(&$form, &$form_state) {
  if (strtotime($form_state['values']['opencrm_dbs_expiry']) === FALSE) {
    form_error($form['opencrm_dbs_expiry'], t('Invalid expiry duration, please check and try again.'));
  }
}

/**
 * Form submission handler for opencrm_dbs_config_form().
 *
 * @see opencrm_dbs_config_form()
 * @see opencrm_dbs_config_form_validate()
 */
function opencrm_dbs_config_form_submit(&$form, &$form_state) {
  variable_set('opencrm_dbs_expiry', $form_state['values']['opencrm_dbs_expiry']);
  variable_set('opencrm_dbs_use_workforces', $form_state['values']['opencrm_dbs_use_workforces']);
  drupal_set_message(t('DBS settings successfully saved.'));
}
