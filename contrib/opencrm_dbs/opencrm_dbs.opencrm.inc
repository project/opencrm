<?php
/**
 * @file
 * OpenCRM hook implementations.
 */

/**
 * Implements hook_opencrm_dedupe_verify_property_info().
 */
function opencrm_dbs_opencrm_dedupe_verify_property_info() {
  return array(
    t('DBS') => array(
      'profile2_dbs:dbs_status' => array(
        'use label' => TRUE,
      ),
      'profile2_dbs:dbs_cert_no' => array(),
    )
  );
}
