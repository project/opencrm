<?php
/**
 * @file
 * Queue worker callbacks for OpenCRM DBS.
 */

/**
 * Expire out of date DBS records.
 *
 * @see opencrm_dbs_expire_dbs()
 */
function _opencrm_dbs_expire_dbs() {
  $limit = 50;

  // Get hold of all DBS profiles that should be expired.
  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'profile2');
  $query->entityCondition('bundle', 'dbs');

  // Get ones that should have expired...
  $query->fieldCondition('dbs_expiry', 'value', date('Y-m-d H:i:s', REQUEST_TIME), '<');
  // .. but haven't yet.
  $query->fieldCondition('dbs_status', 'value', array('dbs_clear', 'disclosure_accepted'), 'IN');

  // Get the oldest ones first.
  $query->fieldOrderBy('dbs_expiry', 'value', 'ASC');

  // Limit to stop processing too may at once.
  $query->range(0, $limit);

  // Iterate over the profiles.
  $result = $query->execute();
  if (!empty($result['profile2'])) {
    foreach (entity_load('profile2', array_keys($result['profile2'])) as $profile) {
      // Set the status to expired.
      $wrapper = entity_metadata_wrapper('profile2', $profile);
      $wrapper->dbs_status = 'dbs_expired';
      $wrapper->save();
    }

    // If we had $limit to process, there may be more, so re-queue.
    if (count($result['profile2']) == $limit) {
      $queue = DrupalQueue::get('opencrm_dbs_expire_dbs');
      $queue->createQueue();
      $queue->createItem(NULL);
    }
  }
}
