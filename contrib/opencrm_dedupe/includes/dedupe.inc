<?php
/**
 * @file
 * Contains the OpenCRMDedupeSearchApiDatabase class.
 */

/**
 * Class for deduping from a Search API Database index.
 */
class OpenCRMDedupe {

  /**
   * The machine name for this finder.
   * @var string
   */
  public $name;

  /**
   * Human readable name for the finder.
   * @var string
   */
  public $label;

  /**
   * Description for the finder.
   * @var string
   */
  public $description;

  /**
   * Whether this finder is disabled.
   */
  public $disabled = FALSE;

  /**
   * The fields to match on.
   * @var array
   */
  public $fields;

  /**
   * Additional settings.
   * @var array
   */
  public $settings;

  /**
   * Store potential duplicates and their exclude status.
   *
   * @param array $duplicates
   *   An array of duplicates. Each item should be an array with 2 party id.
   * @param bool $exclude
   *   The exclude value for this set of duplicates.
   */
  public static function storeDuplicates(array $duplicates, $exclude = NULL) {
    if (isset($exclude)) {
      $exclude = (int) $exclude;
    }

    foreach ($duplicates as $ids) {
      // Put the IDs in order.
      sort($ids);
      $ids = array_combine(array('pid1', 'pid2'), $ids);

      // Set this through a merge query.
      $query = db_merge('opencrm_dedupe_matches')
        ->key($ids);

      if (isset($exclude)) {
        $ids['exclude'] = $exclude;
        $query->updateFields(array('exclude' => $exclude));
      }

      $query->insertFields($ids);
      $query->execute();
    }
  }

  /**
   * Find and store duplicates.
   */
  function run() {
    $query = $this->buildQuery();
    $ids = array();
    foreach ($query->execute()->fetchCol() as $pids) {
      $pids = explode(',', $pids);
      while (count($pids) > 1 && $pid1 = array_shift($pids)) {
        foreach ($pids as $pid2) {
          $ids[] = array($pid1, $pid2);
        }
      }
    }
    OpenCRMDedupe::storeDuplicates($ids);
  }

  /**
   * Build a query for finding a duplicate.
   *
   * @return SelectQuery
   *
   * @throws Exception
   */
  function buildQuery() {
    $query = $base = NULL;
    $index = search_api_index_load('opencrm_dedupe');
    $server = $index->server();

    $connection = explode(':', $server->options['database']);
    $fields = $server->options['indexes'][$index->machine_name];
    $tables = array();

    foreach ($this->fields as $field) {
      if (!isset($fields[$field])) {
        throw new Exception(t('Field @field not found in index @index', array('@field' => $field, '@index' => $index)));
      }

      $field_info = $fields[$field];

      // Start our query/build our join from the base for this field.
      if (!$query) {
        $tables[$field_info['table']] = $base = $field_info['table'];
        /** @var SelectQuery $query */
        $query = Database::getConnection($connection[0], $connection[1])->select($field_info['table'], $base);
      }
      elseif (!isset($tables[$field_info['table']])) {
        $tables[$field_info['table']] = $query->join($field_info['table'], $field_info['table'], "%alias.item_id = {$base}.item_id");
      }
      $alias = $tables[$field_info['table']];

      // Ensure we discount any NULL/empties.
      $query->condition("{$alias}.{$field_info['column']}", '', '!=');
      $query->isNotNull("{$alias}.{$field_info['column']}");

      // Add this field to our group by.
      $query->groupBy("{$alias}.{$field_info['column']}");
    }

    $query->addExpression("GROUP_CONCAT(DISTINCT {$base}.item_id ORDER BY {$base}.item_id ASC)", 'pids');
    $query->having("COUNT(DISTINCT {$base}.item_id) > 1");

    if (!empty($this->settings['hats'])) {
      $hat_alias = $query->join('field_data_party_hat', 'party_hat',
        "%alias.entity_type = :party AND %alias.entity_id = {$base}.item_id",
        array(':party' => 'party'));
      $query->condition("{$hat_alias}.party_hat_hat_name", $this->settings['hats']);
    }

    return $query;
  }

}
