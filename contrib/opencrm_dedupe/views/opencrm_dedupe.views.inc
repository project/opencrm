<?php
/**
 * @file
 * Views integration

/**
 * Implements hook_views_data().
 */
function opencrm_dedupe_views_data() {
  $data = array();

  $data['opencrm_dedupe_matches']['table']['group'] = t('OpenCRM duplicates');

  $data['opencrm_dedupe_matches']['table']['base'] = array(
    'title' => t('OpenCRM duplicates'),
    'help' => t('Contains potential duplicate parties.'),
  );

  $data['opencrm_dedupe_matches']['pid1'] = array(
    'title' => t('Party ID 1'),
    'help' => t('The ID of the first party.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
    'relationship' => array(
      'base' => 'party',
      'base field' => 'pid',
      'handler' => 'views_handler_relationship',
      'label' => t('Duplicate party 1'),
      'help' => t('The first duplicate party.'),
    ),
  );

  $data['opencrm_dedupe_matches']['pid2'] = array(
    'title' => t('Party ID 2'),
    'help' => t('The ID of the second party.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
    'relationship' => array(
      'base' => 'party',
      'base field' => 'pid',
      'handler' => 'views_handler_relationship',
      'label' => t('Duplicate party 2'),
      'help' => t('The second duplicate party.'),
    ),
  );

  $data['opencrm_dedupe_matches']['pid'] = array(
    'title' => t('Party ID'),
    'help' => t('Filter on either party ID.'),
    'argument' => array(
      'handler' => 'opencrm_dedupe_handler_argument_pid',
      'click sortable' => TRUE,
    ),
    'relationship' => array(
      'base' => 'party',
      'base field' => 'pid',
      'handler' => 'opencrm_dedupe_handler_relationship_pid',
      'label' => t('Other party'),
      'help' => t('The other party to the argument. Requires the argument.'),
    ),
  );

  $data['opencrm_dedupe_matches']['exclude'] = array(
    'title' => t('Exclude'),
    'help' => t('Whether this duplicate has been excluded.'),
    'field' => array(
      'handler' => 'views_handler_field_boolean',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_boolean_operator',
      'type' => 'yes-no',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument',
    ),
  );

  $data['opencrm_dedupe_matches']['exclude_form'] = array(
    'title' => t('Exclude form'),
    'help' => t('Checkbox form for excluding duplicates.'),
    'field' => array(
      'handler' => 'opencrm_dedupe_handler_field_exclude_form',
      'click sortable' => TRUE,
      'real field' => 'exclude',
      'additional fields' => array('pid1', 'pid2'),
    ),
  );

  return $data;
}

/**
 * Implements hook_views_pre_render().
 */
function opencrm_dedupe_views_pre_render(&$view) {
  if (substr($view->name, 0, 14) == 'opencrm_dedupe') {
    ctools_include('modal');
    ctools_modal_add_js();
    drupal_add_css(drupal_get_path('module', 'opencrm') . '/opencrm.css');
  }
}
