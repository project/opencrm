<?php

/**
 * Argument handler to pull duplicates from an ID in either direction.
 */
class opencrm_dedupe_handler_argument_pid extends views_handler_argument {

  /**
   * {@inheritdoc}
   */
  function query($group_by = FALSE) {
    $this->ensure_my_table();
    $this->query->add_where_expression(0, "($this->table_alias.pid1 = :pid OR $this->table_alias.pid2 = :pid)", array(':pid' => $this->argument));
  }

  /**
   * {@inheritdoc}
   */
  function title() {
    $party = party_load($this->argument);
    return $party ? check_plain($party->label) : '';
  }

}
