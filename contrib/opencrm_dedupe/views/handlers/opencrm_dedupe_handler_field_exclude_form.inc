<?php

/**
 * @file
 * Contains field handler to set a booking to processed regarding village
 * requirements.
 */

/**
 * A handler to provide a field for village requirements processed flag
 */
class opencrm_dedupe_handler_field_exclude_form extends views_handler_field {

  protected $element_name;

  function init(&$view, &$options) {
    parent::init($view, $options);

    $this->element_name = str_replace(' ', '_', $this->options['id']);
  }

  function render($values) {
    return '<!--form-item-' . $this->element_name . '--' . $this->view->row_index . '-->';
  }

  function views_form(&$form, &$form_state) {
    // The view is empty, abort.
    if (empty($this->view->result)) {
      return;
    }

    $form[$this->element_name] = array(
      '#tree' => TRUE,
    );

    // At this point, the query has already been run, so we can access the results
    foreach ($this->view->result as $row_id => $row) {
      $form[$this->element_name][$row_id]['pid1'] = array(
        '#type' => 'value',
        '#value' => $this->get_value($row, 'pid1'),
      );
      $form[$this->element_name][$row_id]['pid2'] = array(
        '#type' => 'value',
        '#value' => $this->get_value($row, 'pid2'),
      );
      $form[$this->element_name][$row_id]['exclude'] = array(
        '#type' => 'checkbox',
        '#title' => t('Excluded'),
        '#title_display' => 'invisible',
        '#default_value' => $this->get_value($row),
        //'#parents' => array($this->element_name, $row_id),
      );
      $form[$this->element_name][$row_id]['exclude']['#attributes']['title'] = $form[$this->element_name][$row_id]['exclude']['#title'];
    }
  }

  // Submit handler of views_form().
  function views_form_submit($form, &$form_state) {
    $store = array();

    foreach ($form_state['values'][$this->element_name] as $row_id => $values) {
      $store[$values['exclude']][] = array(
        'pid1' => $values['pid1'],
        'pid2' => $values['pid2'],
      );
    }

    foreach ($store as $exclude => $duplicates) {
      OpenCRMDedupe::storeDuplicates($duplicates, (bool) $exclude);
    }
  }
}
