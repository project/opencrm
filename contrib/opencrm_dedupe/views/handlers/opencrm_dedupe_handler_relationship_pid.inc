<?php

/**
 * Views relationship handler for joining to the other party.
 */
class opencrm_dedupe_handler_relationship_pid extends views_handler_relationship {

  public $pid_arg;

  function init(&$view, &$options) {
    parent::init($view, $options);

    foreach ($this->view->display_handler->handlers['argument'] as $id => $handler) {
      if ($handler->options['table'] == 'opencrm_dedupe_matches' && $handler->options['field'] == 'pid' && $handler->relationship == $this->relationship) {
        if (in_array($handler->options['default_action'], array('default', 'not found', 'empty', 'access denied'))) {
          $this->pid_arg = $id;
          break;
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  function validate() {
    $errors = array();

    if (!isset($this->pid_arg)) {
      $errors = array(t('Unable to find a required duplicate pid argument.'));
    }

    return $errors;
  }

  /**
   * {@inheritdoc}
   */
  function query() {
    $this->ensure_my_table();
    $table_alias = $this->table_alias;
    $this->table_alias = FALSE;

    $arg_handler = $this->view->argument[$this->pid_arg];
    $arg = (int) $arg_handler->get_value();

    $this->real_field = "IF({$table_alias}.pid1 = {$arg}, {$table_alias}.pid2, {$table_alias}.pid1)";

    parent::query();

    $this->table_alias = $table_alias;
  }

}
