<?php
/**
 * @file
 * Default rules exports for OpenCRM Dedupe.
 */

/**
 * Implements hook_default_rules_configuration().
 */
function opencrm_dedupe_default_rules_configuration() {
  $configs = array();

  $variables = array(
    'party' => array(
      'type' => 'party',
      'label' => t('Party'),
    ),
    'return' => array(
      'type' => 'text',
      'label' => t('Return'),
      'parameter' => FALSE,
    ),
  );
  $provides = array('return');

  $rule = rule($variables, $provides);
  $rule->label = t('Email prefix');
  $rule->tags = array('OpenCRM Dedupe');
  $rule->active = TRUE;
  $rule->action('opencrm_parse_email_address', array(
    'email_address:select' => 'party:email',
    'parsed_email:var' => 'parsed_email',
    'parsed_email:label' => 'Parsed Email',
  ));
  $rule->action('data_set', array(
    'data:select' => 'return',
    'value:select' => 'parsed-email:user-name',
  ));
  $configs['opencrm_dedupe_email_prefix'] = $rule;

  $rule = rule($variables, $provides);
  $rule->label = t('Start of first name');
  $rule->tags = array('OpenCRM Dedupe');
  $rule->active = TRUE;
  $rule->action('data_set', array(
      'data:select' => 'return',
      'value:select' => 'party:profile2-party-indiv:party-indiv-name:given',
      'value:process' => array(
        'opencrm_string_manip' => array(
          'function' => 'substr',
          'arguments' => '0,2',
        ),
      ),
    ));
  $configs['opencrm_dedupe_indiv_name_start'] = $rule;

  $rule = rule($variables, $provides);
  $rule->label = t('Normalized indiv post code');
  $rule->tags = array('OpenCRM Dedupe');
  $rule->active = TRUE;
  $rule->action('data_set', array(
    'data:select' => 'return',
    'value:select' => 'party:profile2-party-indiv:party-indiv-address:postal-code',
    'value:process' => array(
      'opencrm_string_manip' => array(
        'function' => 'str_replace',
        'arguments' => ' ,',
      ),
    ),
  ));
  $configs['opencrm_dedupe_indiv_post_code'] = $rule;

  $rule = rule($variables, $provides);
  $rule->label = t('Start of org name');
  $rule->tags = array('OpenCRM Dedupe');
  $rule->active = TRUE;
  $rule
    ->condition('data_is', array(
      'data:select' => 'party:party-hat',
      'value' => array('party_org' => 'party_org'),
    ))
    ->action('data_set', array(
      'data:select' => 'return',
      'value:select' => 'party:label',
      'value:process' => array(
        'opencrm_string_manip' => array(
          'function' => 'substr',
          'arguments' => '0,2',
        ),
      ),
    ));
  $configs['opencrm_dedupe_org_name_start'] = $rule;

  $rule = rule($variables, $provides);
  $rule->label = t('Normalized org post code');
  $rule->tags = array('OpenCRM Dedupe');
  $rule->active = TRUE;
  $rule->action('data_set', array(
      'data:select' => 'return',
      'value:select' => 'party:profile2-party-org:party-org-address:postal-code',
      'value:process' => array(
        'opencrm_string_manip' => array(
          'function' => 'str_replace',
          'arguments' => ' ,',
        ),
      ),
    ));
  $configs['opencrm_dedupe_org_post_code'] = $rule;

  return $configs;
}
