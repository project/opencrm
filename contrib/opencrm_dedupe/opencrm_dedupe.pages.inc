<?php
/**
 * @file
 * Page callbacks for OpenCRM Dedupe.
 */

/**
 * Form for merging two parties.
 */
function opencrm_dedupe_party_merge_form($form, &$form_state, $primary_party = NULL, $secondary_party = NULL) {
  // If we weren't given both parties in the URL, show the selection form.
  if (!$primary_party || !$secondary_party) {
    // Show the selection form.
    $form['selection'] = array(
      '#type' => 'fieldset',
      '#title' => t('Select parties to merge'),
      '#description' => t('Enter the IDs of the parties you want to merge.'),
      '#tree' => TRUE,
    );
    $form['selection']['#collapsible'] =
      $form['selection']['#collapsed'] = $form_state['submitted'];

    $form['selection']['primary_party'] = array(
      '#type' => 'textfield',
      '#title' => t('Primary Party Id'),
      '#description' => t('Data from the secondary party will be merged into this party.'),
      '#required' => TRUE,
    );

    $form['selection']['secondary_party'] = array(
      '#type' => 'textfield',
      '#title' => t('Duplicate party id'),
      '#description' => t('Data frm this party will be merged into the primary party.'),
      '#required' => TRUE,
    );

    $form['selection']['actions'] = array('#type' => 'actions');
    $form['selection']['actions']['verify'] = array(
      '#type' => 'submit',
      '#value' => t('Verify merge'),
    );

    $primary_party = !empty($form_state['values']['selection']['primary_party']) ? $form_state['values']['selection']['primary_party'] : NULL;
    $secondary_party = !empty($form_state['values']['selection']['secondary_party']) ? $form_state['values']['selection']['secondary_party'] : NULL;
  }

  // Add our current selection (URL or form) for our merge submission.
  $form['primary_party'] = array(
    '#type' => 'value',
    '#value' => $primary_party,
  );
  $form['secondary_party'] = array(
    '#type' => 'value',
    '#value' => $secondary_party,
  );

  // If we don't have both values, there is nothing more to do.
  if (!$primary_party || !$secondary_party) {
    return $form;
  }

  $primary_party = party_load($form['primary_party']['#value']);
  $secondary_party = party_load($form['secondary_party']['#value']);

  // If we're able to load our parties, show a verification table.
  if ($primary_party && $secondary_party) {
    $primary_wrapper = entity_metadata_wrapper('party', $primary_party);
    $secondary_wrapper = entity_metadata_wrapper('party', $secondary_party);

    // Add some CSS - it's not enough to warrant a stylesheet.
    $form['#attached']['css'][] = drupal_get_path('module', 'opencrm') . '/opencrm.css';

    $form['merge'] = array(
      '#type' => 'container',
    );
    if (isset($form['selection'])) {
      $form['merge']['#states'] = array(
        'visible' => array(
          ':input[name="selection[primary_party]"]' => array('value' => $primary_party->pid),
          ':input[name="selection[secondary_party]"]' => array('value' => $secondary_party->pid),
        )
      );
    }

    $form['merge']['verify'] = array(
      '#theme' => 'table',
      '#header' => array(
        array('header' => FALSE),
        t('<strong>Primary</strong><br/>@label <small>[@pid]</small>', array('@label' => $primary_party->label, '@pid' => $primary_party->pid)),
        t('<strong>Duplicate</strong><br/>@label <small>[@pid]</small>', array('@label' => $secondary_party->label, '@pid' => $secondary_party->pid)),
      ),
      '#rows' => array(),
    );

    $form['merge']['config'] = array();

    // Capture a flag so that custom callbacks can prevent merges.
    $prevent_merge = FALSE;
    foreach (opencrm_dedupe_verify_property_info() as $group => $properties) {
      if ($group) {
        $group_header = array(array(
          'colspan' => 3,
          'header' => TRUE,
          'data' => $group,
        ));
      }

      foreach ($properties as $property => $info) {
        $parts = explode(':', $property);
        $final_part = array_pop($parts);

        $v1 = $primary_wrapper;
        $v2 = $secondary_wrapper;
        foreach ($parts as $part) {
          $v1 = $v1 && $v1->value() && isset($v1->{$part}) ? $v1->{$part} : NULL;
          $v2 = $v2 && $v2->value() && isset($v2->{$part}) ? $v2->{$part} : NULL;

          // If we don't have either value we can skip out.
          if (!$v1->value() || !$v2->value()) {
            continue 2;
          }
        }

        $property_info = $v1->getPropertyInfo($final_part);

        // If not set, pull through the label.
        if (!isset($info['label'])) {
          $info['label'] = $property_info['label'];
        }

        // Get our final value.
        $v1_value = $v2_value = NULL;
        if (!empty($property_info['field']) && $info['render field']) {
          $instance = field_info_instance($v1->type(), $final_part, $v1->getBundle());
          $instance['display']['default']['label'] = 'hidden';

          if ($v1->{$final_part}->value() !== NULL) {
            $v1_value = field_view_field($instance['entity_type'], $v1->value(), $final_part, $instance['display']['default']);
            $v1_value = drupal_render($v1_value);
          }
          if ($v2->{$final_part}->value() !== NULL) {
            $v2_value = field_view_field($instance['entity_type'], $v2->value(), $final_part, $instance['display']['default']);
            $v2_value = drupal_render($v2_value);
          }
        }
        else {
          $method = $info['use label'] ? 'label' : 'value';
          $v1_value = $v1 && $v1->value() && isset($v1->{$final_part}) ? $v1->{$final_part}->{$method}() : NULL;
          $v2_value = $v2 && $v2->value() && isset($v2->{$final_part}) ? $v2->{$final_part}->{$method}() : NULL;

          if (!$info['use label']) {
            // If we don't have a list, wrap it in an array so we can process
            // everything the same.
            $type = $property_info['type'];
            if (substr($type, 0, 5) != 'list<') {
              $v1_value = isset($v1_value) ? array($v1_value) : NULL;
              $v2_value = isset($v2_value) ? array($v2_value) : NULL;
            }
            else {
              $type = entity_property_list_extract_type($type);
            }

            // Now loop over and do any processing on the type.
            foreach (array('v1_value', 'v2_value') as $value) {
              if (is_array($$value)) {
                foreach ($$value as &$item) {
                  switch ($type) {
                    case 'date':
                      $item = format_date($item);
                      break;
                  }
                }
              }
            }

            // And convert back into a string.
            $v1_value = is_array($v1_value) ? implode(', ', $v1_value) : $v1_value;
            $v2_value = is_array($v2_value) ? implode(', ', $v2_value) : $v2_value;
          }
        }

        // If we don't have either value we can skip out.
        if ($v1_value === NULL && $v2_value === NULL) {
          continue;
        }

        // Output the group header if we have one.
        if (isset($group_header)) {
          $form['merge']['verify']['#rows'][$group] = $group_header;
          unset($group_header);
        }

        // Build our row.
        $row = array();
        $row['data'][0] = $info['label'];
        $row['data'][1] = $v1_value;
        $row['data'][2]['data'] = $v2_value;

        // Run the comparison if requested.
        if ($info['compare']) {
          $match = NULL;

          // If a callable, pass onto the custom comparison.
          if (is_callable($info['compare'])) {
            $prevent_this_merge = FALSE;
            $match = $info['compare']($v1, $v2, $prevent_this_merge, $form['merge']['config']);
            $prevent_merge = $prevent_merge || $prevent_this_merge;
          }
          // Otherwise perform a simple string comparison.
          elseif ($v1_value !== NULL && $v2_value !== NULL) {
            if ($info['strip html']) {
              $v1_value = isset($v1_value) ? strip_tags($v1_value) : $v1_value;
              $v2_value = isset($v2_value) ? strip_tags($v2_value) : $v2_value;
            }
            $match = $v1_value == $v2_value;
          }
          elseif ($v1_value === NULL && $v2_value) {
            $match = TRUE;
          }

          if (isset($match)) {
            $row['data'][2]['class'][] = 'opencrm-dedupe-' . ($match ? 'match' : 'conflict');
          }
        }

        // Add our row to the table.
        $form['merge']['verify']['#rows'][$property] = $row;
      }
    }

    $form['merge']['warning'] = array(
      '#type' => 'html_tag',
      '#tag' => 'div',
      '#attributes' => array('class' => array('messages', 'warning')),
      '#value' => t('The colour coding provides an indication of what is likely to happen during the merge process. Results may vary according to specific circumstances.'),
    );

    if ($prevent_merge) {
      $form['merge']['prevented'] = array(
        '#type' => 'html_tag',
        '#tag' => 'div',
        '#attributes' => array('class' => array('messages', 'error')),
        '#value' => t('This merge has been prevented. Please check for any at the top of the page.'),
      );
    }

    if (!empty($form['merge']['config'])) {
      $form['merge']['config']['#type'] = 'fieldset';
      $form['merge']['config']['#title'] = t('Additional settings');
      $form['merge']['config']['#tree'] = TRUE;
    }

    $form['merge']['actions'] = array(
      '#type' => 'actions',
      '#weight' => 100,
      '#access' => !$prevent_merge,
    );

    $form['merge']['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Run merge'),
      '#validate' => array(
        'opencrm_dedupe_party_merge_form_run_validate',
      ),
      '#submit' => array(
        'opencrm_dedupe_party_merge_form_run_submit',
      ),
    );

    // If we want to process immediately, add our batch submission handler.
    if (variable_get('opencrm_dedupe_batch', 1)) {
      $form['merge']['actions']['submit']['#submit'][] = 'opencrm_dedupe_party_merge_form_batch';
    }
  }

  return $form;
}

/**
 * Form validation handler for verifying a merge.
 */
function opencrm_dedupe_party_merge_form_validate(&$form, &$form_state) {
  if (!party_load($form_state['values']['selection']['primary_party'])) {
    form_set_error('selection][primary_party', t('Unable to find party %pid.', array('%pid' => $form_state['values']['primary_party'])));
  }
  if (!party_load($form_state['values']['selection']['secondary_party'])) {
    form_set_error('selection][secondary_party', t('Unable to find party %pid.', array('%pid' => $form_state['values']['secondary_party'])));
  }
}

/**
 * Form submission handler for verifying a merge.
 */
function opencrm_dedupe_party_merge_form_submit(&$form, &$form_state) {
  $form_state['rebuild'] = TRUE;
}


/**
 * Form validation for merging two parties.
 */
function opencrm_dedupe_party_merge_form_run_validate($form, &$form_state) {
  // If we have the selection values, make sure they haven't been changed.
  if (isset($form_state['values']['selection']['primary_party'])) {
    if ($form_state['values']['selection']['primary_party'] != $form_state['values']['primary_party']) {
      form_set_error('selection][primary_party', t('Please verify your new selection.'));
    }
  }
  if (isset($form_state['values']['selection']['secondary_party'])) {
    if ($form_state['values']['selection']['secondary_party'] != $form_state['values']['secondary_party']) {
      form_set_error('selection][secondary_party', t('Please verify your new selection.'));
    }
  }
}

/**
 * Form submission for merging two parties.
 */
function opencrm_dedupe_party_merge_form_run_submit($form, &$form_state) {
  $primary = party_load($form_state['values']['primary_party']);
  $secondary = party_load($form_state['values']['secondary_party']);
  $config = isset($form_state['values']['config']) ? $form_state['values']['config'] : array();

  entity_merge($primary, $secondary, 'party', 0, $config);

  drupal_set_message(t('%secondary has been queued for merging with %primary.', array('%secondary' => $secondary->label, '%primary' => $primary->label)));

  $form_state['redirect'] = 'admin/party/' . $primary->pid;
}

/**
 * Form submission handler for starting the Batch API.
 */
function opencrm_dedupe_party_merge_form_batch($form, &$form_state) {
  $batch = array(
    'operations' => array(
      array('opencrm_dedupe_party_merge_batch_process', array()),
    ),
    'finished' => 'opencrm_dedupe_party_merge_batch_finish',
    'title' => t('Merging parties'),
    'init_message' => t('Preparing to merge parties. Please do not close this window.'),
    'progress_message' => t('Estimated progress @percentage%. Please do not close this window.'),
    'error_message' => t('Error merging parties.'),
    'file' => drupal_get_path('module', 'opencrm_dedupe') . '/opencrm_dedupe.pages.inc',
  );
  batch_set($batch);
}

/**
 * Form submission for processing a batch of merges.
 */
function opencrm_dedupe_party_merge_form_process($form, &$form_state) {
  // Get the time at the start.
  $start_time = microtime(TRUE);

  // Get the queue
  $queue = DrupalQueue::get('entity_merge');
  $count = 0;

  while ((microtime(TRUE) - $start_time) < 29 && $item = $queue->claimItem()) {
    $proc = $item->data;
    $proc->prepare()->go();
    $queue->deleteItem($item);
    $count++;
  }

  drupal_set_message(t('Ran @count merge processes.', array('@count' => $count)));
}

/**
 * Batch API processor for processing party merges.
 */
function opencrm_dedupe_party_merge_batch_process(&$context) {
  if (!isset($context['sandbox']['progress'])) {
    $context['sandbox']['progress'] = 0;
    $context['sandbox']['estimate'] = variable_get('opencrm_dedupe_batch_estimate', 0);
    $context['results']['error_count'] = 0;
  }

  /* @var DrupalQueueInterface $queue */
  $queue = DrupalQueue::get('entity_merge');
  $end = time() + 1;
  $process_count = 0;
  while (time() < $end && ($item = $queue->claimItem())) {
    $process_count++;
    try {
      entity_merge_queue_item_process($item->data);
      $context['sandbox']['progress']++;
      $queue->deleteItem($item);
    }
    catch (Exception $e) {
      watchdog_exception('opencrm_dedupe', $e);
      $context['results']['error_count']++;
    }
  }

  // If we haven't processed any items, we will not set the finish information,
  // thereby finishing the batch processing.
  if ($process_count > 0) {
    // We can only estimate the max number, so we'll take the max of our initial
    // estimate and the sum of processed items and items remaining in the queue.
    $context['sandbox']['max'] = max($context['sandbox']['progress'] + $queue->numberOfItems(), $context['sandbox']['estimate']);
    $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
  }
}

/**
 * Batch API finish callback.
 *
 * Notify user if any errors have occurred.
 */
function opencrm_dedupe_party_merge_batch_finish($success, $results, $operations) {
  if ($success) {
    if ($results['error_count']) {
      $message = t('Merge finished, but @count errors occurred during processing.', array('@count' => $results['error_count']));
      drupal_set_message($message, 'warning');
    }
  }
  else {
    // @todo Display error message.
  }
}
