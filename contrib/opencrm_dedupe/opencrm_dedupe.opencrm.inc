<?php
/**
 * @file
 * OpenCRM hook implementations.
 */

/**
 * Implements hook_opencrm_dedupe_verify_property_info().
 */
function opencrm_dedupe_opencrm_dedupe_verify_property_info() {
  $info = array(
    NULL => array(
      '#weight' => -99,
      'party_hat' => array(
        'render field' => TRUE,
      ),
      'email' => array(),
      'party_indiv_to_org' => array(
        'use label' => TRUE,
      )
    )
  );

  // Add some info from our data sets, checking that they are valid.
  $data_set_info = party_get_data_set_info();
  
  // Individuals.
  if (isset($data_set_info['profile2_party_indiv'])) {
    $properties = array(
      '#weight' => -10,
    );

    // Expose all fields excluding name/email.
    foreach (field_info_instances('profile2', 'party_indiv') as $instance) {
      if (in_array($instance['field_name'], array('party_indiv_name', 'party_indiv_email'))) {
        continue;
      }

      $properties['profile2_party_indiv:' . $instance['field_name']] = array('render field' => TRUE);
    }

    if (!empty($properties)) {
      $info[$data_set_info['profile2_party_indiv']['label']] = $properties;
    }
  }
  
  // Organisations.
  if (isset($data_set_info['profile2_party_org'])) {
    $properties = array(
      '#weight' => -10,
    );

    // Expose all fields excluding name/email.
    foreach (field_info_instances('profile2', 'party_org') as $instance) {
      if (in_array($instance['field_name'], array('party_org_name', 'party_org_email'))) {
        continue;
      }

      $properties['profile2_party_org:' . $instance['field_name']] = array('render field' => TRUE);
    }

    if (!empty($properties)) {
      $info[$data_set_info['profile2_party_org']['label']] = $properties;
    }
  }

  // If party user is enabled, we may have web accounts to deal with.
  if (module_exists('party_user')) {
    $info[t('Web Account')] = array(
      'user:mail' => array(
        'label' => t('Login email'),
        'compare' => 'opencrm_dedupe_compare_account',
      ),
      'user:last_login' => array(
        'compare' => FALSE,
      ),
    );
  }
  
  return $info;
}

/**
 * Callback for comparing web accounts.
 *
 * @see callback_opencrm_dedupe_compare()
 */
function opencrm_dedupe_compare_account($v1, $v2, &$prevent_merge, &$form) {
  if ($v2->raw() !== NULL) {
    drupal_set_message(t('Merging parties with web accounts is not supported. Please ensure only the primary party has a web account.'), 'error');
    $prevent_merge = TRUE;
    return FALSE;
  }
}
