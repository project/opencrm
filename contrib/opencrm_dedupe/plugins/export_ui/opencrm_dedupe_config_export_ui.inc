<?php

/**
 * Define this Export UI plugin.
 */
$plugin = array(
  'schema' => 'opencrm_dedupe_config',  // As defined in hook_schema().
  'access' => 'administer opencrm dedupe',  // Define a permission users must have to access these pages.

  // Define the menu item.
  'menu' => array(
    'menu prefix' => 'admin/config/party',
    'menu item' => 'opencrm-dedupe',
    'menu title' => 'OpenCRM Deduping',
    'menu description' => 'Configure OpenCRM duplicate finders.',
  ),

  // Define user interface texts.
  'title singular' => t('finder'),
  'title plural' => t('finder'),
  'title singular proper' => t('OpenCRM duplicate finder'),
  'title plural proper' => t('OpenCRM duplicate finder'),

  // Define the names of the functions that provide the add/edit forms.
  'form' => array(
    'settings' => 'opencrm_dedupe_config_export_ui_form',
    'submit' => 'opencrm_dedupe_config_export_ui_form_submit',
  ),

  'handler' => array(
    'path' => drupal_get_path('module', 'ctools') . '/plugins/export_ui',
    'class' => 'ctools_export_ui',
  ),
);

/**
 * Define the preset add/edit form.
 */
function opencrm_dedupe_config_export_ui_form(&$form, &$form_state) {
  $finder = $form_state['item'];

  $form['info']['description']['#type'] = 'textfield';

  $form['fields'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Fields that constitute a match'),
    '#description' => t('All fields must match to count as a match.'),
    '#default_value' => $finder->fields,
    '#options' => array(),
  );
  $index = search_api_index_load('opencrm_dedupe');
  $fields = $index->getFields();
  foreach ($fields as $field => $info) {
    if ($info['type'] == 'string' || $info['type'] == 'list<string>') {
      $form['fields']['#options'][$field] = $info['name'];
    }
  }

  $form['settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Settings'),
    '#tree' => TRUE,
  );

  $form['settings']['hats'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Filter by hats'),
    '#description' => t('Optionally filter by hats. If none are selected, potential matches will be found across all hats.'),
    '#options' => party_hat_build_hat_options(NULL, NULL, 'edit'),
    '#default_value' => !empty($finder->settings['hats']) ? $finder->settings['hats']: array(),
  );
}

/**
 * Define the submit function for the add/edit form.
 */
function opencrm_dedupe_config_export_ui_form_submit(&$form, &$form_state) {
  $form_state['values']['fields'] = array_filter($form_state['values']['fields']);
  $form_state['values']['settings']['hats'] = array_filter($form_state['values']['settings']['hats']);
}
