<?php
/**
 * @file
 * OpenCRM Dedupe module.
 */

/**
 * Implements hook_hook_info().
 */
function opencrm_dedupe_hook_info() {
  $hooks['opencrm_dedupe_verify_property_info'] = array('group' => 'opencrm');
  $hooks['opencrm_dedupe_verify_property_info_alter'] = array('group' => 'opencrm');
  $hooks['opencrm_dedupe_finders'] = array('group' => 'opencrm');
  $hooks['opencrm_dedupe_finders_alter'] = array('group' => 'opencrm');
  return $hooks;
}

/**
 * Implements hook_menu().
 */
function opencrm_dedupe_menu() {
  $items['admin/party/merge'] = array(
    'title' => 'Merge Parties',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('opencrm_dedupe_party_merge_form', NULL, NULL),
    'access arguments' => array('opencrm dedupe parties'),
    'file' => 'opencrm_dedupe.pages.inc',
    'type' => MENU_NORMAL_ITEM,
    'menu_name' => 'management',
  );

  $items['admin/party/merge/%ctools_js/%/%'] = array(
    'title' => 'Merge Parties',
    'page callback' => 'opencrm_ajax_form',
    'page arguments' => array(3, 'opencrm_dedupe_party_merge_form', 4, 5),
    'access arguments' => array('opencrm dedupe parties'),
    'theme callback' => 'ajax_base_page_theme',
    'file' => 'opencrm_dedupe.pages.inc',
    'type' => MENU_CALLBACK,
  );

  return $items;
}

/**
 * Implements hook_permission().
 */
function opencrm_dedupe_permission() {
  $perms = array();

  $perms['administer opencrm dedupe'] = array(
    'title' => t('Administer deduping'),
    'description' => t('Configure and manage deduping of parties.'),
  );

  $perms['opencrm dedupe parties'] = array(
    'title' => t('De-duplicate parties'),
    'description' => t('Allow the user to dedupe parties.'),
  );

  return $perms;
}

/**
 * Implements hook_ctools_plugin_api().
 */
function opencrm_dedupe_ctools_plugin_api($owner, $plugin) {
  if ($owner == 'panels_mini' && $plugin == 'panels_default') {
    return array('version' => 1);
  }
}

/**
 * Implements hook_ctools_plugin_directory().
 */
function opencrm_dedupe_ctools_plugin_directory($module, $type) {
  if ($type =='export_ui') {
    return 'plugins/export_ui';
  }
}

/**
 * Implements hook_views_api().
 */
function opencrm_dedupe_views_api() {
  return array(
    'api' => 3,
    'path' => drupal_get_path('module', 'opencrm_dedupe') . '/views',
  );
}

/**
 * Get a list of properties to show on the verification page.
 *
 * @return array
 *   An array of properties to show in the comparison. Outer keys are group
 *   labels (NULL is no group). Keys are property strings (a chain of metadata
 *   wrapper properties separated by :). Values are arrays containing:
 *   - label: Optional string to override the normal label.
 *   - use label: Optionally attempt to use the EntityValueWrapper::label().
 *   - render field: If the property is a field, setting this to TRUE will
 *     render the property via the Field API.
 *   - compare: A boolean indicating whether to run a comparison or a callable
 *     for a custom comparison function. See callback_opencrm_dedupe_compare().
 *   - strip html: TRUE if HTML should be stripped from the basic comparison.
 *     Defaults to TRUE.
 */
function opencrm_dedupe_verify_property_info() {
  $info = module_invoke_all('opencrm_dedupe_verify_property_info');
  drupal_alter('opencrm_dedupe_verify_property_info', $info);

  // Sort everything by weight.
  $weight = 0;
  foreach ($info as &$properties) {
    // Set a default weight on the group.
    $properties += array('#weight' => $weight++ / 100);

    // Set up our defaults on the properties.
    foreach (element_children($properties) as $property) {
      $properties[$property] += array(
        '#weight' => $weight++ / 100,
        'use label' => FALSE,
        'render field' => FALSE,
        'compare' => TRUE,
        'strip html' => TRUE,
      );
    }

    // Sort the propreties.
    uasort($properties, 'element_sort');

    // Remove the weight.
    foreach (element_children($properties) as $property) {
      unset($properties[$property]['#weight']);
    }
  }

  // Sort the group.
  uasort($info, 'element_sort');

  // Remove group weights.
  foreach ($info as &$properties) {
    unset($properties['#weight']);
  }

  return $info;
}

/**
 * Load deduping finders.
 *
 * @param bool $enabled
 *   If FALSE, disabled configs will be included.
 *
 * @return OpenCRMDedupe[]
 */
function opencrm_dedupe_finders($enabled = TRUE) {
  ctools_include('export');
  $return = ctools_export_load_object('opencrm_dedupe_config');
  if ($enabled) {
    foreach ($return as $name => $finder) {
      if (!empty($finder->disabled)) {
        unset($return[$name]);
      }
    }
  }
  return $return;
}

/**
 * Property getter callback for retrieving a value from a dedupe component.
 *
 * @see entity_property_verbatim_get().
 */
function opencrm_dedupe_rule_property_get($data, array $options, $name, $type, $info) {
  if ($result = rules_invoke_component($info['rule'], $data)) {
    $result = reset($result);

    // If this is non-empty text, normalize it.
    if (!empty($result) && entity_property_extract_innermost_type($info['type']) == 'text') {
      $result = strtolower(transliteration_get(utf8_encode($result)));
    }

    return $result;
  }
}

/**
 * Implements hook_rules_config_insert().
 */
function opencrm_dedupe_rules_config_insert($config) {
  opencrm_dedupe_rules_config_postsave($config);
}

/**
 * Implements hook_rules_config_update().
 */
function opencrm_dedupe_rules_config_update($config) {
  opencrm_dedupe_rules_config_postsave($config);
}

/**
 * Postsave callback for rules configs.
 *
 * @see opencrm_dedupe_rules_config_insert()
 * @see opencrm_dedupe_rules_config_update()
 */
function opencrm_dedupe_rules_config_postsave($config) {
  if (isset($config->tags) && in_array('OpenCRM Dedupe', $config->tags)) {
    entity_property_info_cache_clear();
    $index = search_api_index_load('opencrm_dedupe');
    if (!empty($index->options['fields']['opencrm_dedupe__' . $config->name])) {
      $index->reindex();
    }
  }
}

/**
 * Implements hook_opencrm_dedupe_default_config().
 */
function opencrm_dedupe_opencrm_dedupe_default_config() {
  $export =  array();

  $config = new OpenCRMDedupe;
  $config->name = 'indiv_email';
  $config->label = 'Indiv email';
  $config->description = 'Matches individuals on identical email addresses.';
  $config->enabled = TRUE;
  $config->fields = array('email');
  $config->settings = array(
    'hats' => array('party_indiv'),
  );
  $export['indiv_email'] = $config;

  $config = new OpenCRMDedupe;
  $config->name = 'indiv_name_address';
  $config->label = 'Indiv name and address';
  $config->description = 'Matches on same first 2 letters of first name, last name and post code.';
  $config->enabled = TRUE;
  $config->fields = array(
    'rule__opencrm_dedupe_indiv_name_start',
    'profile2_party_indiv:party_indiv_name:family',
    'rule__opencrm_dedupe_indiv_post_code',
  );
  $config->settings = array(
    'hats' => array('party_indiv'),
  );
  $export['indiv_name_address'] = $config;

  $config = new OpenCRMDedupe;
  $config->name = 'org_email';
  $config->label = 'Org email';
  $config->description = 'Matches organisations on identical email addresses.';
  $config->enabled = TRUE;
  $config->fields = array('email');
  $config->settings = array(
    'hats' => array('party_org'),
  );
  $export['org_email'] = $config;

  $config = new OpenCRMDedupe;
  $config->name = 'org_name_address';
  $config->label = 'Org name and address';
  $config->description = 'Matches on same first 2 letters of name and post code.';
  $config->enabled = TRUE;
  $config->fields = array(
    'rule__opencrm_dedupe_org_name_start',
    'rule__opencrm_dedupe_org_post_code',
  );
  $config->settings = array(
    'hats' => array('party_org'),
  );
  $export['org_name_address'] = $config;

  return $export;
}

/**
 * Implements hook_default_search_api_index().
 */
function opencrm_dedupe_default_search_api_index() {
  $items = array();

  // Special Configurable Server
  $items['opencrm_dedupe'] = entity_import('search_api_index', '{
    "name" : "OpenCRM Dedupe",
    "machine_name" : "opencrm_dedupe",
    "description" : "Database index for deduping parties.",
    "server" : "opencrm_dedupe",
    "item_type" : "party",
    "options" : {
      "index_directly" : 0,
      "cron_limit" : "50",
      "fields" : {
        "email" : { "type" : "string" },
        "rule__opencrm_dedupe_indiv_post_code" : { "type" : "string" },
        "rule__opencrm_dedupe_indiv_name_start" : { "type" : "string" },
        "rule__opencrm_dedupe_email_prefix" : { "type" : "string" },
        "rule__opencrm_dedupe_org_name_start" : { "type" : "string" },
        "rule__opencrm_dedupe_org_post_code" : { "type" : "string" },
        "search_api_language" : { "type" : "string" },
        "profile2_party_indiv:party_indiv_name:given" : { "type" : "string" },
        "profile2_party_indiv:party_indiv_name:family" : { "type" : "string" }
      },
      "data_alter_callbacks" : { "party_alter_status_filter" : { "status" : 1, "weight" : "-10", "settings" : [] } },
      "processors" : {
        "search_api_case_ignore" : {
          "status" : 1,
          "weight" : "0",
          "settings" : { "fields" : {
              "email" : true,
              "rule__opencrm_dedupe_indiv_post_code" : true,
              "rule__opencrm_dedupe_name_start" : true,
              "rule__opencrm_dedupe_email_prefix" : true,
              "rule__opencrm_dedupe_org_post_code" : true,
              "profile2_party_indiv:party_indiv_name:given" : true,
              "profile2_party_indiv:party_indiv_name:family" : true
            }
          }
        },
        "search_api_transliteration" : {
          "status" : 1,
          "weight" : "-10",
          "settings" : { "fields" : {
              "email" : true,
              "rule__opencrm_dedupe_name_start" : true,
              "rule__opencrm_dedupe_email_prefix" : true,
              "profile2_party_indiv:party_indiv_name:given" : true,
              "profile2_party_indiv:party_indiv_name:family" : true
            }
          }
        }
      }
    },
    "enabled" : "1",
    "read_only" : "0"
  }');

  return $items;
}

/**
 * Implements hook_default_search_api_server().
 */
function opencrm_dedupe_default_search_api_server() {
  $items = array();

  $items['opencrm_dedupe'] = entity_import('search_api_server', '{
    "name" : "OpenCRM Dedupe Server",
    "machine_name" : "database_server",
    "description" : "",
    "class" : "search_api_db_service",
    "options" : {
      "database" : "default:default",
      "min_chars" : "3"
    },
    "enabled" : "1"
  }');

  return $items;
}

/**
 * Implements hook_cron().
 */
function opencrm_dedupe_cron() {
  // Get hold of our last runs and our queue info.
  $last_run = variable_get(__FUNCTION__, array());
  $cut_off = strtotime('-1 day', REQUEST_TIME);
  $queue = DrupalQueue::get('opencrm_dedupe_finder');

  // Find out what's already in the queue as each item only needs to be in once.
  $existing_queue = db_query('SELECT data FROM {queue} WHERE name = :name', array(':name' => 'opencrm_dedupe_finder'))->fetchCol();
  foreach ($existing_queue as &$item) {
    $finder = unserialize($item);
    $item = $finder->name;
  }

  foreach (opencrm_dedupe_finders() as $finder) {
    if (empty($last_run[$finder->name]) || $last_run[$finder->name] < $cut_off) {
      // Check if it's already in the queue.
      if (!in_array($finder->name, $existing_queue)) {
        $queue->createItem($finder);
      }
    }
  }
}

/**
 * Implements hook_cron_queue_info().
 */
function opencrm_dedupe_cron_queue_info() {
  $queues['opencrm_dedupe_finder'] = array(
    'worker callback' => 'opencrm_dedupe_finder_run',
    'time' => 30,
  );
  return $queues;
}

/**
 * Queue callback to run a finder.
 *
 * @param OpenCRMDedupe $finder
 *   The finder we want to run.
 */
function opencrm_dedupe_finder_run(OpenCRMDedupe $finder) {
  $finder->run();
  $last_run = variable_get('opencrm_dedupe_cron', array());
  $last_run[$finder->name] = time();
  variable_set('opencrm_dedupe_cron', $last_run);
}

/**
 * Implements hook_panels_pane_content_alter().
 */
function opencrm_dedupe_panels_pane_content_alter($content, $pane, $args, $contexts) {
  if ($pane->type == 'views_panes' && $pane->subtype == 'opencrm_dedupe_matches-pane') {
    $query = db_select('opencrm_dedupe_matches', 'm');
    $query->leftJoin('party', 'p1', '%alias.pid = m.pid1 AND %alias.hidden = :hidden', array(':hidden' => 0));
    $query->leftJoin('party', 'p2', '%alias.pid = m.pid2 AND %alias.hidden = :hidden', array(':hidden' => 0));

    $or_pid = db_or()
      ->condition('m.pid1', $args[0])
      ->condition('m.pid2', $args[0]);
    $or_hidden = db_or()
      ->isNotNull('p1.pid')
      ->isNotNull('p2.pid');
    $query->condition('m.exclude', 0)
      ->condition($or_pid)
      ->condition($or_hidden);
    $duplicates = $query->countQuery()->execute()->fetchField();

    if ($duplicates) {
      // Mimic drupal_html_id().
      $id = strtr(drupal_strtolower($content->title), array(' ' => '-', '_' => '-', '[' => '-', ']' => ''));
      $id = preg_replace('/[^A-Za-z0-9\-_]/', '', $id);
      $id = preg_replace('/\-+/', '-', $id);
      drupal_add_css('.ui-tabs-anchor[href="#' . $id . '"]:after{content:"' . $duplicates . '";font-size:.8em;position:relative;top:-.5em;color:#f00}', array('type' => 'inline'));
    }
  }
}
