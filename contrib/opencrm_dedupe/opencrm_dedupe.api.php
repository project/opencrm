<?php
/**
 * @file
 * API documentation for OpenCRM Dedupe.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Expose information on the de-duping merge verification table.
 *
 * @return array
 *   An array of de-duping information. See
 *   opencrm_dedupe_verify_property_info() for the data structure. In addition,
 *   groups and properties can have the '#weight' key to allow adjusting of the
 *   order.
 *
 * @see opencrm_dedupe_verify_property_info()
 * @see hook_opencrm_dedupe_verify_property_info_alter()
 */
function hook_opencrm_dedupe_verify_property_info() {
  return array(
    NULL => array(
      '#weight' => -99,
      'party_hat' => array(
        'render field' => TRUE,
      ),
      'email' => array(),
      'party_indiv_to_org' => array(
        'use label' => TRUE,
      )
    )
  );
}

/**
 * Alter the exposed information on the de-duping merge verification table.
 *
 * @param array $info
 *   The verification property info array to be altered.
 *
 * @see opencrm_dedupe_verify_property_info()
 * @see hook_opencrm_dedupe_verify_property_info()
 */
function hook_opencrm_dedupe_verify_property_info_alter(&$info) {
  $info[NULL]['party_hat']['label'] = t('Custom hat label');
}

/**
 * Callback for comparing deduplication properties.
 *
 * @param mixed $v1
 *   The value for the primary party.
 * @param mixed $v2
 *   The value for the secondary party.
 * @param bool $prevent_merge
 *   Whether we should prevent merging. Set to TRUE to prevent the merge from
 *   going ahead. Defaults to FALSE.
 * @param array $form
 *   A subset of the merge form to
 */
function callback_opencrm_dedupe_compare($v1, $v2, &$prevent_merge, &$form) {

}

/**
 * @} End of "addtogroup hooks"
 */
