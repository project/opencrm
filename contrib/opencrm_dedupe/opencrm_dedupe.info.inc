<?php
/**
 * @file
 * Info hook implementations for OpenCRM Dedupe.
 */

/**
 * Implements hook_entity_property_info_alter().
 */
function opencrm_dedupe_entity_property_info_alter(&$property_info) {
  if (module_exists('rules')) {
    $properties =& $property_info['party']['properties'];

    $conditions = array(
      'plugin' => 'rule',
      'tags' => array('OpenCRM Dedupe'),
      'active' => TRUE,
    );
    foreach (entity_load('rules_config', FALSE, $conditions) as $rule) {
      $provides = $rule->providesVariables();
      $return = reset($provides);
      $properties['rule__' . $rule->name] = array(
        'label' => t('OpenCRM Dedupe: @label', array('@label' => $rule->label)),
        'description' => t('An OpenCRM Deduping property.'),
        'type' => $return['type'],
        'getter callback' => 'opencrm_dedupe_rule_property_get',
        'rule' => $rule->name,
      );
    }
  }
}
