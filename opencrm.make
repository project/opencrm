; Open CRM make file

api = 2
core = 7.x

; UX/UI Improvements

projects[admin_views][version] = 1.6
projects[admin_views][subdir] = contrib

; Utilities

projects[ctools][subdir] = contrib
projects[ctools][version] = 1.14
; Issue #2917353-2: Wizards don't work with ajax modals.
projects[ctools][patch][2917353] = http://www.drupal.org/files/issues/2917353.patch

projects[entity][subdir] = contrib
projects[entity][version] = 1.9
; Issue #2506781-3: Don't always load entity before viewing it in ctool content plugin.
projects[entity][patch][2506781] = http://www.drupal.org/files/issues/2506781-3.patch
; Issue #2649382-3: 'Import' operation form does not update exported entity status
projects[entity][patch][2649382] = http://www.drupal.org/files/issues/2649382-3.patch

projects[flexiform][subdir] = contrib
projects[flexiform][type] = module
projects[flexiform][download][url] = http://git.drupal.org/project/flexiform.git
projects[flexiform][download][revision] = 4302fcb8105646dc9e6d470eac9a433cf3488ab3
projects[flexiform][download][branch] = 7.x-1.x

projects[conditional_fields][subdir] = contrib
projects[conditional_fields][type] = module
projects[conditional_fields][download][url] = http://git.drupal.org/project/conditional_fields.git
projects[conditional_fields][download][revision] = cc5c0e372e4ba3d578f86c0d8c03489853483017
projects[conditional_fields][download][branch] = 7.x-3.x

projects[entity_merge][subdir] = contrib
projects[entity_merge][type] = module
projects[entity_merge][download][url] = http://git.drupal.org/project/entity_merge.git
projects[entity_merge][download][revision] = 094405dd94d79e319a10099df66347ca42d528d2
projects[entity_merge][download][branch] = 7.x-1.x

projects[message][subdir] = contrib
projects[message][version] = 1.11
; Issue #2040735-7: Fix undefined index 'target_bundles'
projects[message][patch][2040735] = http://www.drupal.org/files/issues/notice_undefined-2040735-7.patch
; Issue #195: message_get_purgeable_by_type() returns NULL breaking entire cron
projects[message][patch][195] = https://patch-diff.githubusercontent.com/raw/Gizra/message/pull/196.patch

projects[og][subdir] = contrib
projects[og][version] = 2.9
projects[og][patch][2071583] = http://drupal.org/files/2071583-allow_type_filter_on_og_views_relationship.patch

projects[panels][subdir] = contrib
projects[panels][version] = 3.9

projects[relation][subdir] = contrib
projects[relation][version] = 1.2
; Issue #1717364-18: Ctools relationship plugin
projects[relation][patch][1717364] = http://drupal.org/files/issues/relation-ctools-relationships-plugin-1717364-34.patch

projects[rules][version] = 2.10
projects[rules][subdir] = contrib

projects[views][subdir] = contrib
projects[views][version] = 3.18

projects[views_bulk_operations][subdir] = contrib
projects[views_bulk_operations][version] = 3.4
; Issue #1926788-8: Allow operations to use a finished handler
projects[views_bulk_operations][patch][1926788] = http://www.drupal.org/files/issues/1926788-8.patch

; Fields

projects[addressfield][subdir] = contrib
projects[addressfield][version] = 1.0

projects[email][subdir] = contrib
projects[email][version] = 1.3

projects[entityreference][subdir] = contrib
projects[entityreference][version] = 1.5
; Issue #1989952-1: Allow match to be passed to exposed filters
projects[entityreference][patch][1989952] = http://drupal.org/files/1989952-1-allow_match_on_exposed_filters.patch
; Issue #2098093-1: Allow other modules to provide entityreference selection compatible views tables.
projects[entityreference][patch][2098093] = http://drupal.org/files/2098093-1-allow_non_base_table_views_tables.patch
; Issue #1492260-54: Allow views autocomplete filters.
projects[entityreference][patch][1492260] = http://www.drupal.org/files/issues/1492260-54.patch
; Issue #1468862-26: Add a delta filter in views relationships.
projects[entityreference][patch][1468862] = patches/relationship_delta-1468862-26-local.patch
; Access control on views selection handlers
projects[entityreference][patch][views] = patches/entityreference-views_select_access_check.patch

projects[name][subdir] = contrib
projects[name][download][url] = http://git.drupal.org/project/name.git
projects[name][download][revision] = dbb981c8c4f6d53fc3acb385671d13b1f25ce9ee
projects[name][download][branch] = 7.x-1.x

; Search

projects[facetapi][version] = 1.5
projects[facetapi][subdir] = contrib

projects[search_api][subdir] = contrib
projects[search_api][version] = 1.22
projects[search_api][patch][1123454] = patches/add-add_field-to-sapi-query.patch

projects[search_api_db][subdir] = contrib
projects[search_api_db][version] = 1.6

projects[search_api_solr][subdir] = contrib
projects[search_api_solr][version] = 1.12

; Party

projects[email_confirm][subdir] = contrib
projects[email_confirm][version] = 1.3

projects[party][subdir] = contrib
projects[party][download][url] = http://git.drupal.org/project/party.git
projects[party][download][revision] = bee9a5b5ad70f0bd5b0f8bb2ebc8d0e5e9ff3a46
projects[party][download][branch] = 7.x-1.x

projects[party_extras][subdir] = contrib
projects[party_extras][type] = "module"
projects[party_extras][download][url] = http://git.drupal.org/project/party_extras.git
projects[party_extras][download][revision] = 47e27a79d2981b89201e8a7f6feebad1850ba921
projects[party_extras][download][branch] = 7.x-1.x

projects[party_og][subdir] = contrib
projects[party_og][type] = "module"
projects[party_og][download][url] = http://git.drupal.org/project/party_og.git
projects[party_og][download][revision] = 58e7a4b4a0712de98a8b1deafd7a3286bb6a2f34
projects[party_og][download][branch] = 7.x-2.x

projects[party_simplenews][subdir] = contrib
projects[party_simplenews][type] = "module"
projects[party_simplenews][download][url] = http://git.drupal.org/project/party_simplenews.git
projects[party_simplenews][download][revision] = 00c8de2392dbc12d479ec6855b85157772223b15
projects[party_simplenews][download][branch] = 7.x-2.x

projects[profile2][subdir] = contrib
projects[profile2][version] = 1.4

; Mailing

projects[mailsystem][subdir] = contrib
projects[mailsystem][version] = 2.34

projects[mimemail][subdir] = contrib
projects[mimemail][version] = 1.0

projects[simplenews][subdir] = contrib
projects[simplenews][download][url] = http://git.drupal.org/project/simplenews.git
projects[simplenews][download][revision] = 819029ce2207c8cc3083e9af7acd3ff2b5363307
projects[simplenews][download][branch] = 7.x-2.x
; Issue #2116719-1: Fix problem with rules.
projects[simplenews][patch][2116719] = http://drupal.org/files/2116719-3-fix_rules_and_simplenews.patch
; Issue #1933822-13: Email friendly panels layouts.
projects[simplenews][patch][1933822] = http://drupal.org/files/1933822-13-add_email_friendly_panels_layouts.patch

; OpenCRM Geolocation

projects[search_api_location][subdir] = contrib
projects[search_api_location][download][url] = http://git.drupal.org/project/search_api_location.git
projects[search_api_location][download][revision] = 1657f13901ed8547d6cf6176454916af7f6189ef
projects[search_api_location][download][branch] = 7.x-2.x

projects[geofield][subdir] = contrib
projects[geofield][version] = 2.3

projects[geophp][subdir] = contrib
projects[geophp][version] = 1.7

projects[geocoder][subdir] = contrib
projects[geocoder][download][url] = http://git.drupal.org/project/geocoder.git
projects[geocoder][download][revision] = c8878433f241a1798f00c190210088c46aead9c8
projects[geocoder][download][branch] = 7.x-1.x

; OpenCRM Dedupe
projects[transliteration][subdir] = contrib
projects[transliteration][version] = 3.2
