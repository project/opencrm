<?php

/**
 * Implements hook_rules_data_processor_info().
 *
 * Add string manipulation data processor.
 */
function opencrm_rules_data_processor_info() {
  $processors['opencrm_string_manip'] = array(
    'class' => 'OpenCRMRulesDataProcessorString',
    'type' => 'text',
    'weight' => 0,
  );

  return $processors;
}

/**
 * Implements hook_rules_action_info()
 *
 * Rules actions to help interacton with party data.
 */
function opencrm_rules_action_info() {
  $actions['opencrm_parse_email_address'] = array(
    'label' => t('Parse Email Address'),
    'parameter' => array(
      'email_address' => array(
        'type' => 'text',
        'label' => t('Email Address'),
        'descrtipion' => t('The email address to be parsed.'),
      ),
    ),
    'group' => t('Data'),
    'provides' => array(
      'parsed_email' => array(
        'type' => 'struct',
        'property info' => array(
          'user_name' => array(
            'type' => 'text',
            'label' => t('Username'),
            'getter callback' => 'entity_property_verbatim_get',
          ),
          'domain' => array(
            'type' => 'text',
            'label' => t('Domain'),
            'getter callback' => 'entity_property_verbatim_get',
          ),
        ),
      ),
    ),
    'base' => 'opencrm_rules_parse_email_address',
  );

  return $actions;
}

/**
 * Rules action to parse email addresses.
 */
function opencrm_rules_parse_email_address($email_address) {
  $return = (object) array(
    'user_name' => NULL,
    'domain' => NULL,
  );

  if (empty($email_address)) {
    return $return;
  }

  if ($pos = strpos($email_address, '@')) {
    list($return->user_name, $return->domain) = explode('@', $email_address);
  }

  return array('parsed_email' => $return);
}
