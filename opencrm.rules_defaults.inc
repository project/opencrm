<?php
/**
 * @file
 * opencrm.rules_defaults.inc
 */

/**
 * Implements hook_default_page_manager_pages().
 */
function opencrm_default_rules_configuration() {
  $rules = array();

  // Scan directory for any .panel files
  $files = file_scan_directory(dirname(__FILE__) . '/rules', '/\.json$/', array('key' => 'name'));
  foreach ($files as $file) {
    if ($rule = rules_import(file_get_contents($file->uri))) {
      $rules[$rule->name] = $rule;
    }
  }

  return $rules;
}
