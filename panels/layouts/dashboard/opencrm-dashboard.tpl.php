<div class="panel-display party-dashboard container clearfix" <?php if (!empty($css_id)) { print "id=\"$css_id\""; } ?>>
  <div class="panel-panel panel-left"><?php print $content['left']; ?></div>
  <div class="panel-panel panel-main"><?php print $content['main']; ?></div>
</div>
