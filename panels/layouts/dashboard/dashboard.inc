<?php
$plugin = array(
  'title' => t('Dashboard'),
  'category' => t('Party Dashboard'),
  'icon' => 'dashboard.png',
  'theme' => 'opencrm_dashboard',
  'css' => 'party-dashboard.css',
  'admin css' => 'party-dashboard.css',
  'regions' => array(
    'left' => t('Left'),
    'main' => t('Main'),
  ),
);
