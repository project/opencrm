<?php

$plugin = array(
  'title' => t('View'),
  'category' => t('Party Dashboard'),
  'icon' => 'opencrm_view.png',
  'theme' => 'opencrm_view',
  'css' => 'party-view.css',
  'admin css' => 'party-view.css',
  'regions' => array(
    'left' => t('Top Left'),
    'right' => t('Top Right'),
    'system' => t('System'),
    'tabs' => t('Tabs'),
  ),
);
