<div class="panel-display party-view container flex-container flex-fill" <?php if (!empty($css_id)) { print "id=\"$css_id\""; } ?>>
  <div class="panel-section panel-section-top clearfix">
    <div class="panel-panel panel-left"><?php print $content['left']; ?></div>
    <div class="panel-panel panel-right"><?php print $content['right']; ?></div>
  </div>
  <div class="panel-panel panel-system"><?php print $content['system']; ?></div>
  <div class="panel-panel panel-tabs flex-container flex-fill"><?php print $content['tabs']; ?></div>
</div>
