<?php

/**
 * @file
 * Handle the 'user view' override task.
 *
 * This plugin overrides user/%user and reroutes it to the page manager, where
 * a list of tasks can be used to service this request based upon criteria
 * supplied by access plugins.
 */

/**
 * Specialized implementation of hook_page_manager_task_tasks(). See api-task.html for
 * more information.
 */
function opencrm_user_view_subpage_page_manager_tasks() {
  return array(
    // This is a 'page' task and will fall under the page admin UI
    'task type' => 'page',

    'title' => t('User template (with sub pages)'),

    'admin title' => t('User template (with sub pages)'),
    'admin description' => t('When enabled, this overrides the default Drupal behavior for displaying users at <em>user/%user</em>.'),
    'admin path' => 'user/%user/!subpage',

    // Menu hooks so that we can alter the user/%user menu entry to point to us.
    'hook menu' => 'opencrm_user_view_subpage_menu',
    'hook menu alter' => 'opencrm_user_view_subpage_menu_alter',

    // This is task uses 'context' handlers and must implement these to give the
    // handler data it needs.
    'handler type' => 'context',
    'get arguments' => 'opencrm_user_view_subpage_get_arguments',
    'get context placeholders' => 'opencrm_user_view_subpage_get_contexts',

    // Allow this to be enabled or disabled:
    'disabled' => variable_get('opencrm_user_view_subpage_disabled', TRUE),
    'enable callback' => 'opencrm_user_view_subpage_enable',
    'access callback' => 'opencrm_user_view_subpage_access_check',
  );
}

/**
 * Callback defined by opencrm_user_view_subpage_page_manager_tasks().
 *
 * Alter the user view input so that user view comes to us rather than the
 * normal user view process.
 */
function opencrm_user_view_subpage_menu_alter(&$items, $task) {
  if (variable_get('opencrm_user_view_subpage_disabled', TRUE)) {
    return;
  }

  // Override the user view handler for our purpose.
  if ($items['user/%user']['page callback'] == 'user_view_page' || variable_get('opencrm_override_anyway', FALSE)) {
    $items['user/%user']['page callback'] = 'opencrm_user_view_subpage_page';
    $items['user/%user']['file path'] = $task['path'];
    $items['user/%user']['file'] = $task['file'];
  }
  else {
    // automatically disable this task if it cannot be enabled.
    variable_set('opencrm_user_view_subpage_disabled', TRUE);
    if (!empty($GLOBALS['opencrm_enabling_user_view_subpage'])) {
      drupal_set_message(t('Page manager module is unable to enable user/%user because some other module already has overridden with %callback.', array('%callback' => $callback)), 'error');
    }
  }
}

/**
 * Entry point for our overridden user view.
 *
 * This function asks its assigned handlers who, if anyone, would like
 * to run with it. If no one does, it passes through to Drupal core's
 * user view, which is user_view_page().
 */
function opencrm_user_view_subpage_page($user) {
  // Load my task plugin
  $task = page_manager_get_task('user_view_subpage');
  $subpage = arg();
  array_shift($subpage);
  array_shift($subpage);
  $subpage = implode('/', $subpage);

  // Load the user into a context.
  ctools_include('context');
  ctools_include('context-task-handler');

  // We need to mimic Drupal's behavior of setting the user title here.
  drupal_set_title(format_username($user));
  $contexts = ctools_context_handler_get_task_contexts($task, '', array($user, $subpage));

  $output = ctools_context_handler_render($task, '', $contexts, array($user->uid, $subpage));
  if ($output != FALSE) {
    return $output;
  }

  $function = 'user_view_page';
  foreach (module_implements('page_manager_override') as $module) {
    $call = $module . '_page_manager_override';
    if (($rc = $call('user_view_subpage')) && function_exists($rc)) {
      $function = $rc;
      break;
    }
  }

  // Otherwise, fall back.
  return $function($user);
}

/**
 * Callback to get arguments provided by this task handler.
 *
 * Since this is the user view and there is no UI on the arguments, we
 * create dummy arguments that contain the needed data.
 */
function opencrm_user_view_subpage_get_arguments($task, $subtask_id) {
  return array(
    array(
      'keyword' => 'user',
      'identifier' => t('User being viewed'),
      'id' => 1,
      'name' => 'entity_id:user',
      'settings' => array(),
    ),
    array(
      'keyword' => 'subpage',
      'identifier' => t('Sub page requested'),
      'id' => 2,
      'name' => 'string',
      'settings' => array(
        'use_tail' => 1,
      ),
    ),
  );
}

/**
 * Callback to get context placeholders provided by this handler.
 */
function opencrm_user_view_subpage_get_contexts($task, $subtask_id) {
  return ctools_context_get_placeholders_from_argument(opencrm_user_view_subpage_get_arguments($task, $subtask_id));
}

/**
 * Callback to enable/disable the page from the UI.
 */
function opencrm_user_view_subpage_enable($cache, $status) {
  variable_set('opencrm_user_view_subpage_disabled', $status);

  // Set a global flag so that the menu routine knows it needs
  // to set a message if enabling cannot be done.
  if (!$status) {
    $GLOBALS['opencrm_enabling_user_view_subpage'] = TRUE;
  }
}

/**
 * Callback to determine if a page is accessible.
 *
 * @param $task
 *   The task plugin.
 * @param $subtask_id
 *   The subtask id
 * @param $contexts
 *   The contexts loaded for the task.
 * @return
 *   TRUE if the current user can access the page.
 */
function opencrm_user_view_subpage_access_check($task, $subtask_id, $contexts) {
  return TRUE;
}
