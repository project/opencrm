<?php

/**
 * @file
 * Defines the class for party bulk email.
 * Belongs to the "party_simplenews_bulk" operation type plugin.
 */

class ViewsBulkOperationsPartySimplenewsBulk extends ViewsBulkOperationsBaseOperation {

  /**
   * @overrides ViewsBulkOperationsBaseOperation::getAccessMask()
   */
  public function getAccessMask() {
    return VBO_ACCESS_OP_VIEW;
  }

  /**
   * @overrides ViewsBulkOperationsBaseOperation::adminOptionsForm()
   *
   * Define the path to the entity create form.
   */
  public function adminOptionsForm($dom_id) {
    $form = parent::adminOptionsForm($dom_id);
    $path = $this->getAdminOption('create_path', 'node/add/simplenews');

    $form['create_path'] = array(
      '#type' => 'textfield',
      '#title' => t('Path to form'),
      '#description' => t('Enter the path to the entity create form where the email can be written.'),
      '#default_value' => $path,
    );

    return $form;
  }

  /**
   * Returns the configuration form for the operation.
   * Only called if the operation is declared as configurable.
   *
   * @param $form
   *   The views form.
   * @param $form_state
   *   An array containing the current state of the form.
   * @param $context
   *   An array of related data provided by the caller.
   */
  public function form($form, &$form_state, array $context) {}

  /**
   * Validates the configuration form.
   * Only called if the operation is declared as configurable.
   *
   * @param $form
   *   The views form.
   * @param $form_state
   *   An array containing the current state of the form.
   */
  public function formValidate($form, &$form_state) {}

  /**
   * Handles the submitted configuration form.
   * This is where the operation can transform and store the submitted data.
   * Only called if the operation is declared as configurable.
   *
   * @param $form
   *   The views form.
   * @param $form_state
   *   An array containing the current state of the form.
   */
  public function formSubmit($form, &$form_state) {}

  /**
   * Executes the selected operation on the provided data.
   *
   * @param $data
   *   The data to to operate on. An entity or an array of entities.
   * @param $context
   *   An array of related data (selected views rows, etc).
   * @param $vbo_sandbox
   *   An array of variables to persist between the batch calls.
   */
  public function execute($data, array $context, array &$vbo_sandbox) {
    // Some logic to prepare subscribers.
    $vbo_sandbox['handler_settings']['party_ids'][] = $data->pid;
  }

  /**
   * @overrides ViewsBulkOperationsBaseOperation::finish()
   */
  public function finish($success, &$vbo_sandbox) {
    // On the last step actually send people off to the page.
    $query = array(
      'simplenews_newsletter' => $this->operationInfo['key'],
      'simplenews_handler' => 'party_simplenews_custom',
      'simplenews_handler_settings' => serialize($vbo_sandbox['handler_settings']),
    );
    $options = array(
      'fragment' => 'overlay=' . drupal_encode_path($this->getAdminOption('create_path', 'node/add/simplenews') . '?' . drupal_http_build_query($query)),
    );

    drupal_goto('admin/party', $options);
  }
}
