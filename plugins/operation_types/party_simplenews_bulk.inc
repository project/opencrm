<?php

/**
 * @file
 * CTools plugin. Provides support for using the party_simplenews_custom
 * recipient handler.
 */

$plugin = array(
  'title' => t('Party Bulk Mail'),
  'list callback' => 'party_simplenews_operation_party_simplenews_bulk_list',
  'handler' => array(
    'file' => 'party_simplenews_bulk.class.php',
    'class' => 'ViewsBulkOperationsPartySimplenewsBulk',
  ),
);

/**
 * Returns a prepared list of available newsletters.
 *
 * @param $operation_id
 *   The full, prefixed operation_id of the operation (in this case,
 *   newsletter_id) to return, or NULL to return an array with all operations.
 */
function party_simplenews_operation_party_simplenews_bulk_list($operation_id = NULL) {
  // @todo: Remove this an move into party simplenews.
  if (!module_exists('party_simplenews')) {
    return array();
  }

  // @todo: I should NOT have to clear the cache here!!!!!
  $newsletters = entity_load('simplenews_newsletter', FALSE, array(), TRUE);
  $operations = array();

  foreach ($newsletters as $newsletter) {
    // @todo: Filter out newsletters which can't use the party handler
	  // All operations must be prefixed with the operation type.
    $new_operation_id = 'party_simplenews_bulk::' . $newsletter->newsletter_id;
    $operations[$new_operation_id] = array(
      'operation_type' => 'party_simplenews_bulk',
      // Keep the unprefixed key around, for internal use.
      'key' => $newsletter->newsletter_id,
      'label' => $newsletter->name,
      'parameters' => array('newsletter_id' => $newsletter->newsletter_id),
      'configurable' => FALSE,
      'type' => 'party',
      'aggregate' => FALSE,
    );
  }

  if (isset($operation_id)) {
    return isset($operations[$operation_id]) ? $operations[$operation_id] : FALSE;
  }
  else {
    return $operations;
  }
}
