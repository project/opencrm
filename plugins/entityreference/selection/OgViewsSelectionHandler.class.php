<?php

/**
 * OG selection handler for Views.
 */
class OgViewsSelectionHandler extends EntityReference_SelectionHandler_Views {

  /**
   * Implements EntityReference_SelectionHandler_Views::getInstance().
   */
  public static function getInstance($field, $instance = NULL, $entity_type = NULL, $entity = NULL) {
    return new OgViewsSelectionHandler($field, $instance, $entity);
  }

  /**
   * Override EntityReference_SelectionHandler_Views::settingsForm().
   */
  public static function settingsForm($field, $instance) {
    $form = parent::settingsForm($field, $instance);
    $entity_type = $field['settings']['target_type'];
    $entity_info = entity_get_info($entity_type);

    $bundles = array();
    foreach ($entity_info['bundles'] as $bundle_name => $bundle_info) {
      if (og_is_group_type($entity_type, $bundle_name)) {
        $bundles[$bundle_name] = $bundle_info['label'];
      }
    }

    if (!$bundles) {
      $form['error'] = array(
        '#type' => 'item',
        '#title' => t('Error'),
        '#markup' => t('The selected "Target type" %entity does not have bundles that are a group type', array('%entity' => $entity_info['label'])),
      );
    }
    else {
      $form['view']['view_and_display']['#description'] .= '<p>' . t('Your view must only return the following bundles; @bundles.', array('@bundles' => implode(', ', $bundles))) . '</p>';

      $options = array();
      foreach (og_membership_type_load() as $og_membership) {
        $options[$og_membership->name] = $og_membership->description;
      }
      $form['membership_type'] = array(
        '#type' => 'select',
        '#title' => t('OG membership type'),
        '#description' => t('Select the membership type that will be used for a subscribing user.'),
        '#options' => $options,
        '#default_value' => isset($field['settings']['handler_settings']['membership_type']) ? $field['settings']['handler_settings']['membership_type']: OG_MEMBERSHIP_TYPE_DEFAULT,
        '#required' => TRUE,
      );
    }

    return $form;
  }

}
