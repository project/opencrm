<?php

/**
 * @file
 * Provide a search portal with Facet API support.
 */

$plugin = array(
  'title' => t('OpenCRM Search Portal'),
  'category' => t('OpenCRM'),
  'defaults' => array(
    'description' => '',
    'submit_label' => 'Search',
  ),
  'content type' => 'opencrm_search_portal_content_type',
  'content types' => 'opencrm_search_portal_content_types',
  'render callback' => 'opencrm_search_portal_render',
  'edit form' => 'opencrm_search_portal_edit_form',
);

/**
 * Get a single subtype.
 */
function opencrm_search_portal_content_type($subtype) {
  $types = opencrm_search_portal_content_types();
  if (isset($types[$subtype])) {
    return $types[$subtype];
  }
}

/**
 * Get all subtypes.
 */
function opencrm_search_portal_content_types() {
  $types = module_invoke_all('opencrm_search_portal_types');
  drupal_alter('opencrm_search_portal_types', $types);

  foreach ($types as &$type) {
    $type['title'] = t('Search portal: !title', array('!title' => $type['title']));
  }

  return $types;
}

/**
 * Render callback.
 */
function opencrm_search_portal_render($subtype, $conf, $args, $context) {
  $subtype_info = opencrm_search_portal_content_type($subtype);

  $block = new stdClass();
  $block->title = $subtype_info['title'];

  $form_state = array(
    'build_info' => array(
      'args' => array($subtype_info, $conf),
    ),
  );
  form_load_include($form_state, 'inc', 'opencrm', 'plugins/content_types/opencrm_search_portal');
  $block->content = drupal_build_form('opencrm_search_portal_form', $form_state);
  unset($block->content['form_build_id'], $block->content['form_token'], $block->content['form_id']);

  return $block;
}

/**
 * Form API constructor for the OpenCRM Search Portal form.
 *
 * @param array $subtype
 *   The subtype info array.
 * @param array $conf
 *   Optional configuration for the portal form.
 *
 * @return array
 *   The form array.
 */
function opencrm_search_portal_form($form, &$form_state, $subtype, $conf = array()) {
  // Set this up to go direct to the results.
  $form = array();
  $form['#action'] = url($subtype['path']);
  $form['#method'] = 'GET';

  // Text search.
  $form['search'] = array(
    '#type' => 'textfield',
    '#title' => t('Search'),
    '#title_display' => 'invisible',
    '#name' => $subtype['search_key'],
    '#description' => $conf['description'],
    '#weight' => -1,
    '#attributes' => array(
      'placeholder' => t('Search'),
    )
  );

  // Add our facets.
  $index = search_api_index_load($subtype['index']);
  $searcher = 'search_api@' . $subtype['index'];
  $adapter = facetapi_adapter_load($searcher);
  $processor = $adapter->getUrlProcessor();
  foreach (($conf['facets'] ?? []) as $facet_name => $settings) {
    // Ignore anything not enabled.
    if (empty($settings['enabled'])) {
      continue;
    }

    $facet = facetapi_facet_load($facet_name, $searcher);
    $value_prefix = rawurlencode($facet['field alias']) . ':';
    // If not exposed, add as a hidden element.
    if (empty($settings['expose'])) {
      $form['facets'][$facet_name] = array(
        '#type' => 'hidden',
        '#value' => $value_prefix . $settings['default_value'],
      );
    }
    else {
      $form['facets'][$facet_name] = array(
        '#type' => 'select',
        '#title' => empty($info['override_title']) ? $facet['label'] : $info['title'],
        '#description' => !empty($info['description']) ? $info['description'] : '',
        '#options' => opencrm_search_portal_facet_options($index, $adapter, $facet_name, $value_prefix),
        '#default_value' => $value_prefix . $settings['default_value'],
        '#empty_option' => t('- Any -'),
        '#empty_value' => '',
      );
    }
    $form['facets'][$facet_name]['#name'] = $processor->getFilterKey() . '[]';
  }

  // Submit button.
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => $conf['submit_label'],
    '#weight' => 100,
    '#name' => FALSE,
  );

  return $form;
}

/**
 * Edit form callback.
 */
function opencrm_search_portal_edit_form($form, &$form_state) {
  $subtype = $form_state['subtype'];
  $conf = $form_state['conf'];

  $form['description'] = array(
    '#type' => 'textfield',
    '#title' => t('Description'),
    '#size' => 50,
    '#description' => t('Short help text displayed below the search box.'),
    '#default_value' => $conf['description'],
    '#prefix' => '<div class="clear-block no-float">',
    '#suffix' => '</div>',
  );

  $form['submit_label'] = array(
    '#type' => 'textfield',
    '#title' => t('Search button text'),
    '#description' => t('Enter the text you would like to appear on the search button.'),
    '#default_value' => $conf['submit_label'],
  );

  $form['facets'] = array(
    '#type' => 'fieldset',
    '#title' => t('Exposed facets'),
    '#descriptions' => t('Select which facets you would like to expose on the search form.'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#tree' => TRUE,
  );

  $form['facets']['#searcher'] = 'search_api@' . $subtype['index'];
  $form['facets']['#realm'] = 'block';

  // Grab the index so we can get retrieve information about the fields.
  $index = search_api_index_load($subtype['index']);

  // Get the adaptor and enabled facets.
  $adapter = facetapi_adapter_load($form['facets']['#searcher']);
  $enabled_facets = $adapter->getEnabledFacets($form['facets']['#realm']);

  foreach ($enabled_facets as $facet_name => $facet) {
    $element = array(
      '#type' => 'fieldset',
      '#title' => $facet['label'],
      '#collapsible' => TRUE,
      '#collapsed' => empty($conf['facets'][$facet_name]['enabled']),
    );

    $element['enabled'] = array(
      '#type' => 'checkbox',
      '#title' => t('Enable this Facet'),
      '#description' => t('Enable this facet as a filter on the search.'),
      '#default_value' => !empty($conf['facets'][$facet_name]['enabled']),
    );

    $element['expose'] = array(
      '#type' => 'checkbox',
      '#title' => t('Expose to site visitors'),
      '#description' => t('Expose this facet as a filter on the search form and allow users to change it.'),
      '#default_value' => !empty($conf['facets'][$facet_name]['expose']),
      '#states' => array(
        'visible' => array(
          'input[name="facets[' . $facet_name . '][enabled]"]' => array('checked' => TRUE),
        ),
      ),
    );

    $element['override_title'] = array(
      '#type' => 'checkbox',
      '#title' => t('Override Title'),
      '#description' => t('Override the title of this filter.'),
      '#default_value' => !empty($conf['facets'][$facet_name]['override_title']),
      '#states' => array(
        'visible' => array(
          'input[name="facets[' . $facet_name . '][expose]"]' => array('checked' => TRUE),
        ),
      ),
    );

    $element['title'] = array(
      '#type' => 'textfield',
      '#title' => t('Title'),
      '#default_value' => !empty($conf['facets'][$facet_name]['title']) ? $conf['facets'][$facet_name]['title'] : '',
      '#states' => array(
        'visible' => array(
          'input[name="facets[' . $facet_name . '][override_title]"]' => array('checked' => TRUE),
        ),
      ),
    );

    $element['description'] = array(
      '#type' => 'textfield',
      '#title' => t('Help Text'),
      '#description' => t('Some helpful text to be displayed with the filter in the form.'),
      '#default_value' => !empty($conf['facets'][$facet_name]['description']) ? $conf['facets'][$facet_name]['description'] : '',
      '#states' => array(
        'visible' => array(
          'input[name="facets[' . $facet_name . '][expose]"]' => array('checked' => TRUE),
        ),
      ),
    );

    $element['default_value'] = array(
      '#type' => 'select',
      '#title' => t('Default Value'),
      '#options' => opencrm_search_portal_facet_options($index, $adapter, $facet_name),
      '#empty_option' => '- No Default -',
      '#empty_value' => 0,
      '#default_value' => !empty($conf['facets'][$facet_name]['default_value']) ? $conf['facets'][$facet_name]['default_value'] : NULL,
      '#facet' => $facet,
      '#states' => array(
        'visible' => array(
          'input[name="facets[' . $facet_name . '][enabled]"]' => array('checked' => TRUE),
        ),
      ),
    );

    $form['facets'][$facet_name] = $element;
  }

  return $form;
}

/**
 * Edit form submission handler.
 */
function opencrm_search_portal_edit_form_submit(&$form, &$form_state) {
  $form_state['conf']['description'] = $form_state['values']['description'];
  $form_state['conf']['search_path'] = $form_state['values']['search_path'];
  $form_state['conf']['submit_label'] = $form_state['values']['submit_label'];
  $form_state['conf']['facets'] = $form_state['values']['facets'];
}

/**
 * Get the options for a facet.
 *
 * @param SearchAPIIndex $index
 *   The index we're searching on.
 * @param FacetApiAdapter $adapter
 *   The Facet API adapter we're working with.
 * @param string $facet
 *   The machine name for the facet.
 * @param string $value_prefix
 *   Optional value prefix (useful for building facet queries).
 *
 * @return array
 *   The options array.
 */
function opencrm_search_portal_facet_options(SearchApiIndex $index, FacetapiAdapter $adapter, $facet, $value_prefix = '') {
  $builds = &drupal_static(__FUNCTION__, array());
  $wrapper = $index->entityWrapper();
  if (!isset($builds[$adapter->getId()])) {
    $builds[$adapter->getId()] = NULL;
  }
  $build = &$builds[$adapter->getId()];

  // Attempt to get the wrapper for the
  $property_info = NULL;
  try {
    $property_wrapper = $wrapper;
    $parts = explode(':', $facet);
    $final_part = array_pop($parts);
    foreach($parts as $property) {
      if (isset($property_wrapper->{$property})) {
        $property_wrapper = $wrapper->{$property};
        while (get_class($property_wrapper) == 'EntityListWrapper') {
          $property_wrapper = $property_wrapper->get(0);
        }
      }
    }
    $property_info = $property_wrapper->getPropertyInfo($final_part);
  }
  catch (Exception $e) {
    // Do nothing.
  }

  if (isset($property_info) && !empty($property_info['options list'])) {
    $options = $property_wrapper->{$final_part}->optionsList();
  }
  else {
    if (!isset($build)) {
      // Prepare a search so we can build default value forms.
      $searchQuery = search_api_query($index->id);
      $searchQuery->execute();
      $query_plugins = $adapter->loadQueryTypePlugins();
      $adapter->initActiveFilters($searchQuery);
      $adapter->processActiveItems();
      $build = $query_plugins[$facet]->build();
    }
    $options = call_user_func($facet['map callback'], array_keys($build), $facet['map options']);
  }

  // Add our value prefix.
  $return = array();
  foreach($options as $value => $label) {
    $return[$value_prefix . $value] = $label;
  }
  return $return;
}
