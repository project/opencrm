<?php

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'single' => TRUE,
  'icon' => 'icon_node_form.png',
  'title' => t('Party form'),
  'description' => t('Embedded party form.'),
  'required context' => new ctools_context_required(t('Party'), 'entity:party'),
  'category' => t('Party'),
);

function opencrm_party_form_content_type_render($subtype, $conf, $panel_args, &$context) {
  $block = new stdClass();

  $form_state = array();

  if ($conf['form_type'] == 'review') {
    $form_id = 'opencrm_party_review_form';
    $form_state['build_info']['args'] = array(entity_metadata_wrapper('party', $context->data));
    form_load_include($form_state, 'inc', 'opencrm', 'opencrm.pages');
  }
  else {
    $form_id = 'party_' . $conf['form_type'] . '_form';
    $form_state['build_info']['args'] = array($context->data);
    form_load_include($form_state, 'inc', 'party', 'party.pages');
  }

  $block->content = drupal_build_form($form_id, $form_state);

  // Hide the cancel button.
  $block->content['actions']['cancel']['#access'] = FALSE;

  $block->title = drupal_get_title();
  if (!empty($conf['override_title'])) {
    $block->content['title'] = array(
      '#theme' => 'html_tag',
      '#tag' => 'h4',
      '#value' => $block->title,
      '#weight' => -10,
    );
  }

  return $block;
}

function opencrm_party_form_content_type_admin_title($subtype, $conf, $context) {
  return t('Party @action form', array('@action' => $conf['form_type']));
}

function opencrm_party_form_content_type_edit_form($form, &$form_state) {
  // provide a blank form so we have a place to have context setting.
  $form['form_type'] = array(
    '#type' => 'select',
    '#title' => t('Form type'),
    '#options' => array(
      'edit' => t('Edit'),
      'archive' => t('Archive'),
      'delete' => t('Delete'),
      'review' => t('Review'),
    ),
    '#default_value' => isset($form_state['conf']['form_type']) ? $form_state['conf']['form_type']: FALSE,
  );
  return $form;
}

function opencrm_party_form_content_type_edit_form_submit(&$form, &$form_state) {
  $form_state['conf']['form_type'] = $form_state['values']['form_type'];
}
