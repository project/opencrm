<?php

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'single' => TRUE,
  'icon' => 'icon_node_form.png',
  'title' => t('Node form simplenews settings'),
  'description' => t('Simplenews settings on the Node form.'),
  'required context' => new ctools_context_required(t('Form'), 'node_form'),
  'category' => t('Form'),
);

function opencrm_node_form_simplenews_content_type_render($subtype, $conf, $panel_args, &$context) {
  $block = new stdClass();
  $block->module = t('node_form');

  $block->title = t('Mailing information');
  $block->delta = 'simplenews_settings';

  if (isset($context->form)) {
    if (!empty($context->form['simplenews'])) {
      $block->content['simplenews'] = $context->form['simplenews'];
      if (isset($block->content['simplenews']['#group'])) {
        unset($block->content['simplenews']['#pre_render']);
        unset($block->content['simplenews']['#theme_wrappers']);
        $block->content['simplenews']['#type'] = '';
      }

      // Set access to false on the original rather than removing so that
      // vertical tabs doesn't clone it. I think this is due to references.
      $context->form['simplenews']['#access'] = FALSE;
    }
  }
  else {
    $block->content = t('Mailing information.');
  }
  return $block;
}

function opencrm_node_form_simplenews_content_type_admin_title($subtype, $conf, $context) {
  return t('"@s" node form simplenews information', array('@s' => $context->identifier));
}

function opencrm_node_form_simplenews_content_type_edit_form($form, &$form_state) {
  // provide a blank form so we have a place to have context setting.
  return $form;
}
