<?php

/**
 * @file
 * Plugin to provide access control/visibility based user context.
 */

$plugin = array(
  'title' => t("User: Dashboard Access"),
  'description' => t('Control access by whether user can view Dashboard pages.'),
  'callback' => 'opencrm_user_dashboard_access_check',
  'settings form' => 'opencrm_user_dashboard_access_settings',
  'summary' => 'opencrm_user_dashboard_access_summary',
  'required context' => new ctools_context_required(t('User'), 'user'),
);

/**
 * Settings form
 */
function opencrm_user_dashboard_access_settings($form, &$form_state, $conf) {
  return $form;
}

/**
 * Check for access
 */
function opencrm_user_dashboard_access_check($conf, $context) {
  return user_view_access($context->data);
}

/**
 * Provide a summary description based upon the specified context
 */
function opencrm_user_dashboard_access_summary($conf, $context) {
  return t('User can access User Dashboard pages');
}
