<?php

/**
 * @file
 * Plugin to provide access control/visibility based user context.
 */

$plugin = array(
  'title' => t("Search API Index: Index is active"),
  'description' => t('Control access by whether a search index is active.'),
  'callback' => 'opencrm_search_api_index_active_check',
  'settings form' => 'opencrm_search_api_index_active_settings',
  'settings form submit' => 'opencrm_search_api_index_active_settings_submit',
  'summary' => 'opencrm_search_api_index_active_summary',
);

/**
 * Settings form
 */
function opencrm_search_api_index_active_settings($form, &$form_state, $conf) {
  $indexes = entity_load('search_api_index');
  $options = array();
  foreach ($indexes as $index) {
    $options[$index->machine_name] = $index->name;
  }

  $form['settings']['index'] = array(
    '#type' => 'select',
    '#title' => t('Index'),
    '#options' => $options,
    '#default_value' => !empty($conf['index']) ? $conf['index'] : '',
    '#required' => TRUE,
  );

  return $form;
}

/**
 * Settings form
 */
function opencrm_search_api_index_active_settings_submit($form, &$form_state) {}

/**
 * Check for access
 */
function opencrm_search_api_index_active_check($conf, $context) {
  $index = search_api_index_load($conf['index']);
  return ($index && $index->enabled);
}

/**
 * Provide a summary description based upon the specified context
 */
function opencrm_search_api_index_active_summary($conf, $context) {
  return t('@index is active', array('@index' => $conf['index']));
}
