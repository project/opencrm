<?php

/**
 * @file
 * Plugin to provide access control/visibility based user context.
 */

$plugin = array(
  'title' => t("User: Can Edit Party Group"),
  'description' => t('Control access by whether a user is an editor of the party group.'),
  'callback' => 'opencrm_user_can_edit_party_group_check',
  'settings form' => 'opencrm_user_can_edit_party_group_settings',
  'summary' => 'opencrm_user_can_edit_party_group_summary',
  'required context' => array(
    new ctools_context_required(t('User'), 'user'),
    new ctools_context_required(t('Group Party'), 'entity:party'),
  ),
);

/**
 * Settings form
 */
function opencrm_user_can_edit_party_group_settings($form, &$form_state, $conf) {
  return $form;
}

/**
 * Check for access
 */
function opencrm_user_can_edit_party_group_check($conf, $contexts) {
  $user = array_shift($contexts)->data;
  $party = array_shift($contexts)->data;

  return in_array('editor', og_get_user_roles('party', $party->pid, $user->uid));
}

/**
 * Provide a summary description based upon the specified context
 */
function opencrm_user_can_edit_party_group_summary($conf, $context) {
  return t('User can edit party group.');
}
