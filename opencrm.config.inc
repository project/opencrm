<?php
/**
 * @file
 * Contains configuration pages and forms for the opencrm module.
 */

/**
 * Define pages.
 */
function opencrm_config_menu() {
  $items = array();

  $items['admin/config/party/opencrm'] = array(
    'title' => 'OpenCRM Configuration',
    'description' => 'Configure settings for OpenCRM.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('opencrm_config_form'),
    'access arguments' => array('configure opencrm'),
    'weight' => 0,
    'file' => 'opencrm.config.inc',
  );

  return $items;
}

/**
 * Form callback for the OpenCRM Settings form.
 */
function opencrm_config_form($form, &$form_state) {
  // Search API Server config.
  $servers = entity_load('search_api_server');
  $server_add_access = user_access('administer search_api');

  if ($servers) {
    // Set up options
    $options = array();
    foreach ($servers as $server) {
      $options[$server->machine_name] = $server->name;
    }

    // Prepare message.
    $message = t('Please select which Search API Server you wish to use for the party index.');
    if ($server_add_access) {
      $message .= ' ' . t('You can add more servers by clicking !link.', array('!link' => l('here', 'admin/config/search/search_api/add_server')));
    }

    $form['search_server'] = array(
      '#title' => t('Search API Server'),
      '#description' => $message,
      '#type' => 'select',
      '#options' => $options,
      '#default_value' => variable_get('opencrm_search_api_server', NULL),
    );
  }
  else {
    $message =  t('There are current no search servers configured for this site. The Party Dashboard requires a Search API Server to function properly.');
    if ($server_add_access) {
      $message .= ' ' . t('You can add a server by clicking !link.', array('!link' => l('here', 'admin/config/search/search_api/add_server')));
    }

    $form['search_server'] = array(
      '#title' => t('Search API Server'),
      '#markup' => $message,
      '#type' => 'markup',
    );
  }

  $form['actions'] = array(
    '#type' => 'actions',
  );

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Save Configuration',
  );

  return $form;
}

/**
 * Validate the config form.
 */
function opencrm_config_form_validate($form, &$form_state) {
  $name = empty($form_state['values']['search_server']) ? FALSE : $form_state['values']['search_server'];
  if ($name && !entity_load_single('search_api_server', $name)) {
    form_set_error('search_server', t('Invalid Server'));
  }
}

/**
 * Submit the config form.
 */
function opencrm_config_form_submit($form, &$form_state) {
  $name = empty($form_state['values']['search_server']) ? FALSE : $form_state['values']['search_server'];
  if ($name && $name != variable_get('opencrm_search_api_server')) {
    variable_set('opencrm_search_api_server', $name);
    drupal_flush_all_caches();
  }
}
