<?php
/**
 * @file
 * Party open crm hook implementations.
 */

/**
 * Implements hook_opencrm_facet_namespace_patterns().
 */
function opencrm_opencrm_facet_namespace_patterns() {
  return array(
    'search_api@party_index' => array(
      'party_dashboard' => "admin/party\nadmin/party/*",
    ),
  );
}

/**
 * Implements hook_opencrm_text_formats().
 */
function opencrm_opencrm_text_formats() {
  return array(
    'html' => array(
      'full_html', // Provided by the standard install profile.
      'panopoly_html_text', // Provided by panopoly_wysiwyg.
    ),
    'filtered' => array(
      'filtered_html', // Provided by the standard install profile.
      'panopoly_wysiwyg_text', // Fallback provided by panopoly_wysiwyg.
    ),
    'wysiwyg' => array(
      'panopoly_wysiwyg_text', // Provided by panopoly_wysiwyg.
      'filtered_html', // Fallback provided by the standard install profile.
    ),
  );
}

/**
 * Implements hook_opencrm_modal_pane_links().
 */
function opencrm_opencrm_modal_pane_links($context) {
  $links = array();

  if (isset($context['pane']->type) && $context['pane']->type == 'attached_entity') {
    $data_sets = party_get_data_set_info();
    $data_set = $data_sets[$context['pane']->subtype]['path element'];
    $pid = $context['args'][0];
    $delta = 0;

    // Append modal edit link to title.
    $links['edit'] = array(
      'url' => 'admin/party/nojs/attached-entity/' . $data_set . '/' . $pid . '/' . $delta,
      'text' => t('Edit'),
      'alt' => t('Edit attached entity'),
    );
  }

  // Party simplenews subscription.
  if (module_exists('party_simplenews')) {
    if (drupal_match_path(current_path(), 'admin/party/*')) {
      if (isset($context['pane']->subtype) && substr($context['pane']->subtype, 0, 30) == 'party_simplenews_subscriptions') {
        $pid = $context['args'][0];

        $links['edit'] = array(
          'url' => 'party_simplenews/modal/nojs/' . $pid . '/subscriptions/edit',
          'text' => t('Edit'),
          'alt' => t('Edit subscriptions'),
        );
      }
    }
  }

  return $links;
}

/**
 * Implements hook_opencrm_modal_pane_links_alter().
 */
function opencrm_opencrm_modal_pane_links_alter(&$links, $context) {
  // If there isn't a specific one, attempt to provide a generic edit for
  // the entity_view content type.
  if (empty($links['edit'])) {
    if (isset($context['pane']->type) && $context['pane']->type == 'entity_view') {
      // Perform a form check like entity_form().
      $info = entity_get_info($context['pane']->subtype);
      if (isset($info['form callback']) || entity_ui_controller($context['pane']->subtype)) {
        $links['edit'] = array(
          'url' => 'admin/party/nojs/entity/' . $context['pane']->subtype . '/' . $context['args'][0],
          'text' => t('Edit'),
          'alt' => t('Edit @title', array($context['pane']->title)),
        );
      }
    }
  }
}

/**
 * Implements hook_opencrm_search_portal_types().
 */
function opencrm_opencrm_search_portal_types() {
  $types = array();

  $types['party_dashboard'] = array(
    'title' => t('Party dashboard'),
    'category' => t('OpenCRM'),
    'path' => 'admin/party',
    'index' => 'party_index',
    'search_key' => 'search'
  );

  return $types;
}
