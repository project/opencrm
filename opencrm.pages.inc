<?php
/**
 * @file
 *  Custom Pages defined by the opencrm module.
 */

/**
 * Page Callback for the Email preview page.
 */
function opencrm_email_preview($node) {
  // Only work on newsletter emails.
  if ($node->type != 'simplenews') {
    drupal_not_found();
  }

  if (!empty($node->panelizer['email_html']->display)) {
    $node->panelizer['email_html']->pipeline = 'ipe';
    $node->panelizer['email_html']->display->renderer = 'ipe';
  }

  return node_view($node, 'email_html');
}

/**
 * Page Callback to request rights to edit an organisation.
 */
function opencrm_request_edit_page($org, $indiv = NULL) {
  global $user;

  opencrm_request_editor_status($org, $indiv);
  drupal_goto('user/' . $user->uid . '/related-organisations');
}

/**
 * Page callback for marking a party for review.
 */
function opencrm_party_review($party, $review, $js, $token = NULL) {
  // Check the action makes sense.
  $wrapper = entity_metadata_wrapper('party', $party);
  if ($wrapper->party_needs_review->value() == $review) {
    return MENU_ACCESS_DENIED;
  }

  if ($js) {
    ctools_include('ajax');
    ctools_include('modal');
  }

  // If we have a token, validate and process immediately.
  if ($token) {
    if (!drupal_valid_token($token, 'party_review:' . $party->pid)) {
      return MENU_ACCESS_DENIED;
    }

    $wrapper->party_needs_review = $review;
    $wrapper->save();

    if ($js) {
      return array(
        '#type' => 'ajax',
        '#commands' => array(
          ctools_ajax_command_redirect('admin/party/' . $party->pid),
        ),
      );
    }
    else {
      drupal_goto('admin/party/' . $party->pid);
    }
  }

  // Otherwise output a confirm form.
  $form_state = array();
  $form_state['build_info']['args'] = array($wrapper);
  form_load_include($form_state, 'inc', 'opencrm', 'opencrm.pages');

  if ($js) {
    $form_state['ajax'] = TRUE;
    if ($review) {
      $form_state['title'] = t('Do you want to mark %party for review?', array(
        '%party' => $party->label,
      ));
    }
    else {
      $form_state['title'] = t('Do you want to un-mark %party for review?', array(
        '%party' => $party->label,
      ));
    }

    return array(
      '#type' => 'ajax',
      '#commands' => ctools_modal_form_wrapper('opencrm_party_review_form', $form_state),
    );
  }
  else {
    return drupal_build_form('opencrm_party_review_form', $form_state);
  }
}

/**
 * Form constructor for the party mark for review form.
 *
 * @param $party
 *   The wrapped party we are acting on.
 *
 * @see opencrm_party_review_form_submit().
 */
function opencrm_party_review_form($form, &$form_state, $party) {
  $form['#party'] = $party;
  $form['needs_review'] = array(
    '#type' => 'value',
    '#value' => $party->party_needs_review->value() ? 0: 1,
  );
  if ($form['needs_review']['#value']) {
    $question = t('Do you want to mark %party for review?', array('%party' => $party->label()));
    $description = t('This will show a message on the party dashboard and request the user to update their details next time they log in.');
  }
  else {
    $question = t('Do you want to un-mark %party for review?', array('%party' => $party->label()));
    $description = '';
  }
  return confirm_form($form, $question, 'admin/party/' . $party->getIdentifier(), $description);
}

/**
 * Submission handler for opencrm_party_review_form().
 */
function opencrm_party_review_form_submit(&$form, &$form_state) {
  $form['#party']->party_needs_review = $form_state['values']['needs_review'];
  $form['#party']->save();
  $form_state['redirect'] = 'admin/party/' . $form['#party']->getIdentifier();
}

/**
 * Form constructor for the leave organisation form.
 *
 * @param Party $party
 *   The group party we want to leave membership of.
 * @param $account
 *   Optionally an user who's membership we want to end. If not given, the
 *   current user will be used.
 *
 * @see opencrm_party_organisation_leave_form_submit().
 */
function opencrm_party_organisation_leave_form($form, &$form_state, $party, $account = NULL) {
  if (!$account) {
    global $user;
    $account = $user;
  }

  $form['#group'] = $party;
  $form['#account'] = $account;
  $form['#member'] = party_user_get_party($account);

  $question = t('Are you sure you want to leave %group?', array('%group' => $party->label));
  $path = 'user/' . $account->uid . '/related-organisations';
  return confirm_form($form, $question, $path, '', t('Leave'), t('Cancel'));
}

/**
 * Submission handler for opencrm_party_organisation_leave_form().
 */
function opencrm_party_organisation_leave_form_submit($form, &$form_state) {
  og_ungroup('party', $form['#group']->pid, 'party', $form['#member']->pid);
  $form_state['redirect'] = 'user/' . $form['#account']->uid . '/related-organisations';
}

/**
 * Party View Callback.
 */
function opencrm_party_single_view($party, $view_mode = 'modal') {
  return entity_view('party', array($party), $view_mode);
}
